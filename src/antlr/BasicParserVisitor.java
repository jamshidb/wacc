// Generated from ./BasicParser.g4 by ANTLR 4.4

package antlr;
import frontend.Type;
import frontend.TypeList;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BasicParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BasicParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code ret}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRet(@NotNull BasicParser.RetContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprUnary}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprUnary(@NotNull BasicParser.ExprUnaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam(@NotNull BasicParser.ParamContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprIntLiteral}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprIntLiteral(@NotNull BasicParser.ExprIntLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code block}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(@NotNull BasicParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprAnd}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprAnd(@NotNull BasicParser.ExprAndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code rhsExpr}
	 * labeled alternative in {@link BasicParser#assign_rhs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRhsExpr(@NotNull BasicParser.RhsExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(@NotNull BasicParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code skip}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSkip(@NotNull BasicParser.SkipContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprOR}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprOR(@NotNull BasicParser.ExprORContext ctx);
	/**
	 * Visit a parse tree produced by the {@code rhsCall}
	 * labeled alternative in {@link BasicParser#assign_rhs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRhsCall(@NotNull BasicParser.RhsCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprSumDiff}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprSumDiff(@NotNull BasicParser.ExprSumDiffContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#unary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary(@NotNull BasicParser.UnaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code free}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFree(@NotNull BasicParser.FreeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code println}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrintln(@NotNull BasicParser.PrintlnContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprIdent}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprIdent(@NotNull BasicParser.ExprIdentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprBoolLiteral}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprBoolLiteral(@NotNull BasicParser.ExprBoolLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code read}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRead(@NotNull BasicParser.ReadContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#func}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc(@NotNull BasicParser.FuncContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifstat}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfstat(@NotNull BasicParser.IfstatContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#array_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_type(@NotNull BasicParser.Array_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#pair_elem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPair_elem(@NotNull BasicParser.Pair_elemContext ctx);
	/**
	 * Visit a parse tree produced by the {@code rhsArray}
	 * labeled alternative in {@link BasicParser#assign_rhs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRhsArray(@NotNull BasicParser.RhsArrayContext ctx);
	/**
	 * Visit a parse tree produced by the {@code rhsPair}
	 * labeled alternative in {@link BasicParser#assign_rhs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRhsPair(@NotNull BasicParser.RhsPairContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declaration}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(@NotNull BasicParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprCompare}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprCompare(@NotNull BasicParser.ExprCompareContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprBrackets}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprBrackets(@NotNull BasicParser.ExprBracketsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code rhsPairElem}
	 * labeled alternative in {@link BasicParser#assign_rhs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRhsPairElem(@NotNull BasicParser.RhsPairElemContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprCharLiteral}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprCharLiteral(@NotNull BasicParser.ExprCharLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#base_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBase_type(@NotNull BasicParser.Base_typeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprPairLiteral}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprPairLiteral(@NotNull BasicParser.ExprPairLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#pair_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPair_type(@NotNull BasicParser.Pair_typeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code print}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(@NotNull BasicParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exit}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExit(@NotNull BasicParser.ExitContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#assign_lhs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_lhs(@NotNull BasicParser.Assign_lhsContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#param_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam_list(@NotNull BasicParser.Param_listContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whiledo}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhiledo(@NotNull BasicParser.WhiledoContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignment}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(@NotNull BasicParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#arg_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArg_list(@NotNull BasicParser.Arg_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#array_elem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_elem(@NotNull BasicParser.Array_elemContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprArrayElement}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprArrayElement(@NotNull BasicParser.ExprArrayElementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#pair_elem_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPair_elem_type(@NotNull BasicParser.Pair_elem_typeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code sequence}
	 * labeled alternative in {@link BasicParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSequence(@NotNull BasicParser.SequenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(@NotNull BasicParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link BasicParser#array_liter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_liter(@NotNull BasicParser.Array_literContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprMulDiv}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprMulDiv(@NotNull BasicParser.ExprMulDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprStrLiteral}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprStrLiteral(@NotNull BasicParser.ExprStrLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprEquality}
	 * labeled alternative in {@link BasicParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEquality(@NotNull BasicParser.ExprEqualityContext ctx);
}