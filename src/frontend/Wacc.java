package frontend;

import antlr.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;


import backend.DataList;
import backend.WaccCodeGenVisitor;
import backend.instructions.Instruction;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;


public class Wacc {
	public static void main(String[] args) throws Exception {
        // Get the input code and as an Antlr stream
		ANTLRInputStream input;
        String filename = null;
		if (args.length == 1) {
			File file = new File(args[0]);
			if (!file.exists()) {
				System.err
						.println("Input Error, failed to open file: " + args[0]);
				return;
			}
            filename = file.getName();
			FileInputStream fileStream = new FileInputStream(file);
			input = new ANTLRInputStream(fileStream);
		} else {
			input = new ANTLRInputStream(System.in);
		}
        // Lex and parse the input
		BasicLexer lexer = new BasicLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		BasicParser parser = new BasicParser(tokens);

        // Create error listener to listen for syntax errors and exit if any found
		parser.removeErrorListeners();
		WaccSyntaxErrorListener waccErrListener = new WaccSyntaxErrorListener();
		parser.addErrorListener(waccErrListener);
		ParseTree tree = parser.program();
		
		if (waccErrListener.getFoundError()) {
			System.exit(100); // syntax error
		}

        // Visit the tree and check for syntax errors
		SyntaxVisitor syntaxVisitor = new SyntaxVisitor();
		syntaxVisitor.visit(tree);

        // Create basic structures and visit the tree to check for semantic errors
		HashMap<String, FunctionList> functionMap = new HashMap<String, FunctionList>();
        SymbolTable top = new SymbolTable(null, null);
        DataList dataList = new DataList();
        WaccSemanticVisitor waccVisitor = new WaccSemanticVisitor(top, dataList, functionMap);
		waccVisitor.visit(tree);

        // Visit the tree to produce assembly code
        WaccCodeGenVisitor waccCodeGenVisitor = new WaccCodeGenVisitor(top, dataList);
        waccCodeGenVisitor.visit(tree);
        // Output the assembly code into a file
        if (filename != null) {
            try {
                printInstructionsToFile(filename, waccCodeGenVisitor.getInstructions());
            } catch (FileNotFoundException err) {
                System.err.println(err);
            }
        }
        else {
        	printInstructions(waccCodeGenVisitor.getInstructions());
        }
	}
	
	private static void printInstructionsToFile(String filename, ArrayList<Instruction> i) throws FileNotFoundException{
		PrintWriter out = new PrintWriter(filename.replaceAll("wacc", "s"));
		for(Instruction instr: i){			
			out.print(instr);
		}
		out.close();
	}


	private static void printInstructions(ArrayList<Instruction> i){
		for(Instruction instr : i){
			System.out.println(instr);
		}
	}
}
