package frontend;

public enum PrimitiveTypes implements Type {
	BOOL, INT, CHAR, STRING, ANY;

	
	@Override
	public boolean equalsType(Type a) {
		if (this == ANY) { 
			return true;
		}

        if (a instanceof ArrayType) {
            ArrayType a2 = (ArrayType) a;
            return  a2.getElementType() == CHAR && this == STRING;
        }

		if (!(a instanceof PrimitiveTypes)) {
			return false;
		}
		PrimitiveTypes that = (PrimitiveTypes) a;
		return (this==that || that==PrimitiveTypes.ANY);
	}
	
    public int getTypeSize(){
    	if (this == BOOL || this == CHAR){
    	return 1;
    	}
    	return 4;
    }

}
