package frontend;

import java.util.ArrayList;

public class TypeList extends ArrayList<Type> {
	
	public TypeList(Type first) {
		this.add(first);
	}
	
	public TypeList() {
		super();
	}

	public Type getFirst() { 
		return this.get(0);
	}
	
	@Override
	public boolean contains(Object o) {
		if (!(o instanceof Type)) {
			return false;
		}
		Type otherType = (Type) o;
		for (Type type : this) {
			if (type.equalsType(otherType)) {
				return true;
			}
		}
		return false;
		
	}

}
