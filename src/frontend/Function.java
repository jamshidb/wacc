package frontend;

import java.util.List;

public class Function {
	private Type returnType;
	private TypeList params;

	public Function(TypeList params, Type returnType) {
		if (params == null) {
			this.params = new TypeList();
		} else {
			this.params = params;
		}
		this.returnType = returnType;
	}

	public Type getReturnType() {
		return returnType;
	}

	public TypeList getParams() {
		return params;
	}
	
	@Override
	public boolean equals(Object o){
		Function func = (Function) o;
		TypeList otherParams = func.getParams();
		if (!returnType.equalsType(func.getReturnType())) {
			return false;
		}
	
		return argsEqual(otherParams);
	}

	public boolean argsEqual(TypeList argList) {
		if (params.size() != argList.size()) {
			return false;
		}
		for (int i = 0; i < params.size(); i++) {
			if (!params.get(i).equalsType(argList.get(i))) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "Function with params: " + params + ", return type: " + returnType;
	}
}
