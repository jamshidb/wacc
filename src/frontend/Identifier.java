package frontend;

public class Identifier {
    private Type type;
    private boolean isDeclared = false;

    public Identifier(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }
    
    public String toString() { 
    	return ("type: " + type);
    }

    public boolean isDeclared() {
        return isDeclared;
    }

    public void setDeclared(boolean declared) {
        this.isDeclared = declared;
    }
}
