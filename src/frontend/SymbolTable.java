package frontend;

import java.util.*;

public class SymbolTable {
	ArrayList<SymbolTable> children = new ArrayList<SymbolTable>();
	SymbolTable parent;
	HashMap<String, Identifier> identifiers;
	Type returnType;
	private int declaredBytes = 0; // keeps track of number of bytes declared in current scope
	private int childIndex = 0; // used for incrementing through child symbol tables
	private int cumulativeArguments = 0;

	SymbolTable(SymbolTable st, Type returnType) {
		this.parent = st;
		this.identifiers = new LinkedHashMap<String, Identifier>();
		this.returnType = returnType;
	}

    // resets indices for all children so they can be iterated over again
	public void resetAllChildren(){
		resetChildIndex();
		for (SymbolTable child: children){
			child.resetChildIndex();
			child.resetAllChildren();
		}
	}

	public void resetChildIndex(){
		childIndex = 0;
	}

	public SymbolTable getParent() {
		return this.parent;
	}

	public Type getReturnType() {
		return returnType;
	}

	public int getOffset(String varName) {
		int offset = 0;
		SymbolTable current = this;
		while (current != null) {
			List<Map.Entry<String, Identifier>> identifiers = new ArrayList<>(current.getIdentifiers().entrySet());
			for (int i = identifiers.size()-1; i >= 0; i--) {
                // check if current identifier is the one you're looking for and is declared
				if (varName.equals(identifiers.get(i).getKey()) &&
                        identifiers.get(i).getValue().isDeclared()) {
					return offset + cumulativeArguments ;
				}
				offset += identifiers.get(i).getValue().getType().getTypeSize();
			}
			current = current.parent;
		}
		return -1;

	}
	
	public HashMap<String, Identifier> getIdentifiers(){
		return identifiers;
	}

    public void addIdentifier(String name, Identifier id) {
        identifiers.put(name, id);
        declaredBytes += id.getType().getTypeSize();
    }

	public void setChildren(SymbolTable child) {
		children.add(child);
	}

	public SymbolTable getChild() {
		return children.get(childIndex++);
	}

    public void addCumulativeArgs(int n) {
        cumulativeArguments =+ n;
    }

    public void resetCumulativeArgs(){
        cumulativeArguments = 0;
    }

	// checks if identifier is defined in the current local scope.
	public Identifier lookupCurrLevel(String name) {
		return this.identifiers.get(name);
	}

	public Identifier lookupAllLevels(String name) {
		SymbolTable st = this;
		Identifier id;
		while (st != null) {
			id = st.lookupCurrLevel(name);
			if (id != null) {
				return id;
			}
			st = st.getParent();
		}
		return null;
	}
	
	public int getDeclaredBytes() {
        return declaredBytes;
	}

    public int getCumulativeBytes() {
        int cumulativeBytes = 0;
        SymbolTable current = this;
        if (current.getParent() == null) {
            return current.getDeclaredBytes();
        }
        while (current.getParent().getParent() != null) {
            cumulativeBytes += current.getDeclaredBytes();
            current = current.getParent();
        }
        return cumulativeBytes;
    }

	public String toString() {
		return identifiers.toString();
	}
}