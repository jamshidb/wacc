package frontend;

public class ArrayType implements Type {
	private Type elementType;
    private int size;

    public ArrayType() {
        this.elementType = PrimitiveTypes.ANY;
        this.size = 0;
    }

	public ArrayType(Type elementType) {
		this.elementType = elementType;
	}

    public ArrayType(Type elementType, int size) {
        this.elementType = elementType;
        this.size = size;
    }

	public Type getElementType() {
		return elementType;
	}

    public int getSize() {
        return size;
    }
    
    public int getTypeSize(){
    	return 4;
    }

    @Override
	public boolean equalsType(Type a) {
		if (a instanceof PrimitiveTypes) {
            if (a == PrimitiveTypes.ANY) {
                return true; // an array counts as an 'ANY' type.
            } else if (a == PrimitiveTypes.STRING) {
                return elementType == PrimitiveTypes.CHAR;
            }
		}
		if (a instanceof ArrayType) {
			ArrayType a2 = (ArrayType) a;
			return (this.getElementType().equalsType(a2.getElementType()));
		}
		
		return false;
	}

	@Override
	public String toString() {
		return elementType.toString() + "[]";
	}
	
	

}
