package frontend;


import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class WaccSyntaxErrorListener extends BaseErrorListener {
	private boolean foundError;

	@Override
	public void syntaxError(Recognizer<?, ?> recognizer,
			Object offendingSymbol, int line, int charPositionInLine,
			String msg, RecognitionException e) {
		    System.err.println("line " + line + ":" + charPositionInLine 
				+ ": " + msg);

		foundError = true;
	}

	public boolean getFoundError() {
		return foundError;
	}
}
