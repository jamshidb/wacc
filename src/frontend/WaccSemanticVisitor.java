package frontend;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;

import antlr.BasicParser;
import antlr.BasicParser.Param_listContext;
import antlr.BasicParser.PrintContext;
import antlr.BasicParser.PrintlnContext;
import antlr.BasicParserBaseVisitor;
import backend.DataList;

public class WaccSemanticVisitor extends BasicParserBaseVisitor<Type> {

	private SymbolTable symboltable;
	private DataList data;
	// maps function names to each function.
	private HashMap<String, FunctionList> functionMap;


	
	public WaccSemanticVisitor(SymbolTable top, DataList dataList, HashMap<String, FunctionList> functionMap) {
		this.symboltable = top;
		this.data = dataList;
		this.functionMap = functionMap;
	}

	@Override
	public Type visitExprAnd(BasicParser.ExprAndContext ctx) {
		Type leftType = visit(ctx.expr(0));
		Type rightType = visit(ctx.expr(1));
		// AND must have bools on both sides.
		if (!rightType.equalsType(PrimitiveTypes.BOOL)) {
			error("Semantic Error at " + ctx.stop.getLine() + ":"
					+ ctx.stop.getCharPositionInLine()
					+ " -- Expected type: BOOL, given type: " + rightType);
			System.exit(200);
		}
		if (!leftType.equalsType(PrimitiveTypes.BOOL)) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- Expected type: BOOL, given type: " + leftType);
			System.exit(200);
		}
		return PrimitiveTypes.BOOL;
	}

	@Override
	public Type visitExprOR(BasicParser.ExprORContext ctx) {
		Type leftType = visit(ctx.expr(0));
		Type rightType = visit(ctx.expr(1));
		// OR must have bools on both sides.
		if (!rightType.equalsType(PrimitiveTypes.BOOL)) {
			error("Semantic Error at " + ctx.stop.getLine() + ":"
					+ ctx.stop.getCharPositionInLine()
					+ " -- Expected type: BOOL, given type: " + rightType);
			System.exit(200);
		}
		if (!leftType.equalsType(PrimitiveTypes.BOOL)) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- Expected type: BOOL, given type: " + leftType);
			System.exit(200);
		}
		return PrimitiveTypes.BOOL;
	}

	@Override
	public Type visitDeclaration(BasicParser.DeclarationContext ctx) {
		Type expected = visit(ctx.type());
		if (ctx.assign_rhs() instanceof BasicParser.RhsCallContext) {
			BasicParser.RhsCallContext call = (BasicParser.RhsCallContext) ctx
					.assign_rhs();
			visit(ctx.assign_rhs());
            TypeList returnValues = ctx.assign_rhs().returnTypes;
			if (!returnValues.contains(expected)) {
				error("Semantic Error at " + ctx.start.getLine() + ":"
						+ ctx.start.getCharPositionInLine()
						+ " -- Mismatched types. Function "
						+ call.IDENT().getText() + " does not return "
						+ expected);
				System.exit(200);
			}

		} else {
			Type rhs = visit(ctx.assign_rhs());
			if (!rhs.equalsType(expected)) {
				error("Semantic Error at " + ctx.start.getLine() + ":"
						+ ctx.start.getCharPositionInLine()
						+ " -- Mismatched types. Cannot assign type: " + rhs
						+ " to type: " + expected);
				System.exit(200);
			}
		}
		String ident = ctx.IDENT().getText();
		// Variables can be redeclared in a more local scope so this doesn't
		// need to lookupAllLevels.
		if (this.symboltable.lookupCurrLevel(ident) != null) {
			error("Semantic Error at " + ctx.IDENT().getSymbol().getLine()
					+ ":" + ctx.IDENT().getSymbol().getCharPositionInLine()
					+ " -- Identifier: " + ident + " is already declared");
			System.exit(200);
		} else {
			this.symboltable.addIdentifier(ident, new Identifier(expected));
		}
		
		
		return null;
	}

	@Override
	public Type visitAssignment(BasicParser.AssignmentContext ctx) {
		Type expected = visit(ctx.assign_lhs());
		if (ctx.assign_rhs() instanceof BasicParser.RhsCallContext){
			BasicParser.RhsCallContext call = (BasicParser.RhsCallContext) ctx.assign_rhs();
            visit(ctx.assign_rhs());
			TypeList returnValues = ctx.assign_rhs().returnTypes;
			if (!returnValues.contains(expected)) {
				error("Semantic Error at " + ctx.start.getLine() + ":"
						+ ctx.start.getCharPositionInLine()
						+ " -- Mismatched types. Function " + call.IDENT().getText()
						+ " does not return " + expected);
				System.exit(200);
			}
		} else {
			Type rhs = visit(ctx.assign_rhs());
			if (!expected.equalsType(rhs)) {
				error("Semantic Error at " + ctx.start.getLine() + ":"
						+ ctx.start.getCharPositionInLine()
						+ " -- Mismatched types. Cannot assign a: " + rhs
						+ " to a: " + expected);
				System.exit(200);
			}
		}
		return null;
	}

	@Override
	public Type visitRead(BasicParser.ReadContext ctx) {
		Type expr = visit(ctx.assign_lhs());
		readTypeCheck(expr);
		// can only read into types char, string and int.
		if (!(expr.equalsType(PrimitiveTypes.CHAR)
				|| expr.equalsType(PrimitiveTypes.STRING) || expr
					.equalsType(PrimitiveTypes.INT))) {
			error("Semantic Error at " + ctx.stop.getLine() + ":"
					+ ctx.stop.getCharPositionInLine()
					+ " -- Cannot read into type: " + expr);
			System.exit(200);
		}
		return null;
	}

	private void readTypeCheck(Type exprType) {
		if (exprType.equalsType(PrimitiveTypes.INT) && !data.hasDecimalData()) {
			data.setDecimalData();
		}
		if (exprType.equalsType(PrimitiveTypes.CHAR) && !data.hasBooleanData()) {
			data.setCharData();
		}
	}

	@Override
	public Type visitUnary(BasicParser.UnaryContext ctx) {
		if (ctx.NOT() != null) {
			return PrimitiveTypes.BOOL;
		} else if (ctx.MINUS() != null) {
			return PrimitiveTypes.INT;
		} else if (ctx.LEN() != null) {
			return new ArrayType();
		} else if (ctx.ORD() != null) {
			return PrimitiveTypes.CHAR;
		} else {
			return PrimitiveTypes.INT;
		}
	}

	@Override
	public Type visitRet(BasicParser.RetContext ctx) {
		// return new TypeList(type) for current scope is stored in symbol table
		// (only
		// functions return).
		if (symboltable.getReturnType() == null) {
			error("Semantic Error at " + ctx.RETURN().getSymbol().getLine()
					+ ":" + ctx.RETURN().getSymbol().getCharPositionInLine()
					+ " -- Null return new TypeList(type)");
			System.exit(200);
		}
		if (!symboltable.getReturnType().equalsType(
				visit(ctx.expr()))) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- The given return new TypeList(type): "
					+ symboltable.getReturnType()
					+ " does not match expected type: "
					+ visit(ctx.expr()));
			System.exit(200);
		}
		return null;
	}

	@Override
	public Type visitExprBrackets(BasicParser.ExprBracketsContext ctx) {
		return visit(ctx.expr());
	}

	@Override
	public Type visitExprBoolLiteral(BasicParser.ExprBoolLiteralContext ctx) {
		return PrimitiveTypes.BOOL;
	}

	@Override
	public Type visitExprIntLiteral(BasicParser.ExprIntLiteralContext ctx) {
		return PrimitiveTypes.INT;
	}

	@Override
	public Type visitExprCharLiteral(BasicParser.ExprCharLiteralContext ctx) {
		return PrimitiveTypes.CHAR;
	}

	@Override
	public Type visitExprStrLiteral(BasicParser.ExprStrLiteralContext ctx) {
		data.add(ctx.getText());
		return PrimitiveTypes.STRING;
	}

	@Override
	public Type visitExprIdent(BasicParser.ExprIdentContext ctx) {
		Identifier exprIdent = symboltable.lookupAllLevels(ctx.IDENT()
				.getText());
		if (exprIdent == null) {
			error("Semantic Error at " + ctx.IDENT().getSymbol().getLine()
					+ ":" + ctx.IDENT().getSymbol().getCharPositionInLine()
					+ " -- Identifier: " + ctx.IDENT().getText()
					+ " is not in symboltable.");
			System.exit(200);
		}
		return exprIdent.getType();
	}

	@Override
	public Type visitExprSumDiff(BasicParser.ExprSumDiffContext ctx) {
		if (!data.hasArithmeticData()) {
			data.setArithmeticData();
		}
		Type leftType = visit(ctx.expr(0));
		Type rightType = visit(ctx.expr(1));
		// can only addType or subtract integers.
		if (!rightType.equalsType(PrimitiveTypes.INT)) {
			error("Semantic Error at " + ctx.stop.getLine() + ":"
					+ ctx.stop.getCharPositionInLine()
					+ " -- Expected type: INT, given type: " + rightType);
			System.exit(200);
		}
		if (!leftType.equalsType(PrimitiveTypes.INT)) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- Expected type: INT, given type: " + leftType);
			System.exit(200);
		}
		return PrimitiveTypes.INT;
	}

	@Override
	public Type visitExprCompare(BasicParser.ExprCompareContext ctx) {
		Type leftType = visit(ctx.expr(0));
		Type rightType = visit(ctx.expr(1));
		// This function is for the < and > operators, which can only compare
		// ints and chars.
		if (leftType.equalsType(rightType)
				&& (!leftType.equalsType(PrimitiveTypes.INT) && !leftType
						.equalsType(PrimitiveTypes.CHAR))) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- Incopatible type: " + leftType);
			System.exit(200);
		}
		return PrimitiveTypes.BOOL;
	}

	@Override
	public Type visitExprPairLiteral(BasicParser.ExprPairLiteralContext ctx) {
		// a pair literal does not refer to any actual pair and thus can be a
		// pair of any types.
		return new PairType(PrimitiveTypes.ANY, PrimitiveTypes.ANY);
	}

	@Override
	public Type visitExprArrayElement(
			BasicParser.ExprArrayElementContext ctx) {
		// indexes into array so type is whatever type the array element is.
		return visit(ctx.array_elem());
	}

	@Override
	public Type visitExprMulDiv(BasicParser.ExprMulDivContext ctx) {
		if (ctx.MULT() != null) {
			if (!data.hasArithmeticData()) {
				data.setArithmeticData();
			}
		} else {
			if (!data.hasModDivData()) {
				data.setModDivData();
			}
		}
		Type leftType = visit(ctx.expr(0));
		Type rightType = visit(ctx.expr(1));
		// can only multiply and divide ints.
		if (!rightType.equalsType(PrimitiveTypes.INT)) {
			error("Semantic Error at " + ctx.stop.getLine() + ":"
					+ ctx.stop.getCharPositionInLine()
					+ " -- Expected type: INT, given type: " + rightType);
			System.exit(200);
		}
		if (!leftType.equalsType(PrimitiveTypes.INT)) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- Expected type: INT, given type: " + leftType);
			System.exit(200);
		}
		return PrimitiveTypes.INT;
	}

	@Override
	public Type visitPair_elem_type(BasicParser.Pair_elem_typeContext ctx) {
		Type type;
		if (ctx.base_type() != null) {
			type = visit(ctx.base_type());
		} else if (ctx.array_type() != null) {
			type = visit(ctx.array_type());
		} else { // inner element is another pair
			type = new PairType(PrimitiveTypes.ANY, PrimitiveTypes.ANY);
		}
		return type;
	}

	@Override
	public Type visitExprEquality(BasicParser.ExprEqualityContext ctx) {
		Type leftType = visit(ctx.expr(0));
		Type rightType = visit(ctx.expr(1));
		if (!leftType.equalsType(rightType)) {
			error("Semantic Error at " + ctx.stop.getLine() + ":"
					+ ctx.start.getCharPositionInLine() + " -- Expected type: "
					+ leftType + ", given type: " + rightType);
			System.exit(200);
		}
		return PrimitiveTypes.BOOL;
	}

	@Override
	public Type visitExprUnary(BasicParser.ExprUnaryContext ctx) {
		Type expr = visit(ctx.expr());
		BasicParser.UnaryContext unaryCtx = ctx.unary();
		Type expected = visit(unaryCtx);
		if (!expr.equalsType(expected)) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine() + " -- Expected type: "
					+ expected + ", given type: " + expr);
			System.exit(200);
		}

		// return new TypeList(type) depends on the type of the unary operator.
		if (unaryCtx.NOT() != null) {
			return PrimitiveTypes.BOOL;
		} else if (unaryCtx.MINUS() != null) {
			if (!data.hasArithmeticData()) {
				data.setArithmeticData();
			}
			return PrimitiveTypes.INT;
		} else if (unaryCtx.LEN() != null) {
			return PrimitiveTypes.INT;
		} else if (unaryCtx.ORD() != null) {
			return PrimitiveTypes.INT;
		} else {
			return PrimitiveTypes.CHAR;
		}
	}

	@Override
	public Type visitPair_type(BasicParser.Pair_typeContext ctx) {
		return new PairType(visit(ctx.pair_elem_type(0)), visit(ctx.pair_elem_type(1)));
	}

	@Override
	public Type visitRhsExpr(BasicParser.RhsExprContext ctx) {
		return visit(ctx.expr());
	}

	@Override
	public Type visitRhsArray(BasicParser.RhsArrayContext ctx) {
		return visit(ctx.array_liter());
	}

	@Override
	public Type visitRhsPair(BasicParser.RhsPairContext ctx) {
		PairType pair = new PairType(visit(ctx.expr(0)),
                visit(ctx.expr(1)));
		return pair;
	}

	@Override
	public Type visitRhsPairElem(BasicParser.RhsPairElemContext ctx) {
		return visit(ctx.pair_elem());
	}

	@Override
	// Visitor for when assigning the result of a function call.
	public Type visitRhsCall(BasicParser.RhsCallContext ctx) {
		if (!functionMap.containsKey(ctx.IDENT().getText())) { // function must
																// exist
			error("Semantic Error at " + ctx.IDENT().getSymbol().getLine()
					+ ":" + ctx.IDENT().getSymbol().getCharPositionInLine()
					+ " -- frontend.Function: " + ctx.IDENT().getText()
					+ " does not exist");
			System.exit(200);
		}
		FunctionList funcList = functionMap.get(ctx.IDENT().getText());
		TypeList argList = new TypeList();
		// need to check the provided arguments match the parameter types the
		// function expects.
		if (ctx.arg_list() != null) {
			for (BasicParser.ExprContext expr : ctx.arg_list().expr()) {
				argList.add(visit(expr));
			}
		}
		int argSize = argList.size();
		if (!(funcList.hasArgLength(argSize))) {
			error("Semantic Error at" + ctx.IDENT().getSymbol().getLine() + ":"
					+ ctx.IDENT().getSymbol().getCharPositionInLine()
					+ " -- Incorrect amount of arguments for function: "
					+ ctx.IDENT().getText());
			System.exit(200);
		}

		if (!funcList.validArgTypes(argList)) {
			error("Semantic Error at " + ctx.IDENT().getSymbol().getLine()
					+ ":" + ctx.IDENT().getSymbol().getCharPositionInLine()
					+ " function: " + ctx.IDENT().getText()
					+ " is undefined for the given types " + argList);
			System.exit(200);
		}
		
		// determine the return type by looking at declaration or assignment the call is used in
		Type retType;
		ParserRuleContext assign = ctx.getParent();
		if (assign instanceof BasicParser.DeclarationContext) {
			BasicParser.DeclarationContext decCtx = (BasicParser.DeclarationContext) assign;
			retType = visit(decCtx.type());
		} else {
			BasicParser.AssignmentContext assignCtx = (BasicParser.AssignmentContext) assign;
			retType = visit(assignCtx.assign_lhs());
		}
		Function function = new Function(argList, retType);
		ctx.funcName = funcList.getFuncName(function);
        ctx.returnTypes = funcList.getReturnTypes(argList);
		
		return null;
	}

	@Override
	public Type visitArray_elem(BasicParser.Array_elemContext ctx) {
		if (!data.hasArrayOutOfBounds()) {
			data.setArrayOutOfBounds();
		}
		Identifier ident = symboltable.lookupAllLevels(ctx.IDENT().getText());
		if (ident == null) {
			error("Semantic Error at " + ctx.IDENT().getSymbol().getLine()
					+ ":" + ctx.IDENT().getSymbol().getCharPositionInLine()
					+ " -- Undefined Identifier: " + ctx.IDENT().getText());
			System.exit(200);
		}
		Type identType = ident.getType();
		for (BasicParser.ExprContext expr : ctx.expr()) { // will be multiple
															// expressions if
															// multiple
															// dimensional
															// array.
			if (!visit(expr).equalsType(PrimitiveTypes.INT)) { // the
				// expressions
				// are indices,
				// thus
				// integers.
				error("Semantic Error at" + ctx.start.getLine() + ":"
						+ ctx.start.getCharPositionInLine()
						+ " -- Expected index type: Integer, given type: "
						+ visit(expr));
				System.exit(200);
			}
			if (identType.equalsType(PrimitiveTypes.STRING)) {
				identType = new ArrayType(PrimitiveTypes.CHAR);
			}
			// For each dimension in the array, the current type needs to be
			// array (because it's being indexed into).
			if (!(identType instanceof ArrayType)) {
				error("Semantic Error at "
						+ ctx.IDENT().getSymbol().getLine()
						+ ":"
						+ ctx.start.getCharPositionInLine()
						+ " -- Expected Identifier type: frontend.ArrayType, given type: "
						+ identType);
				System.exit(200);
			}
			// The next type is the current array's inner element type.
			// This should be an array type if loop is not on the last expr
			// because there being more exprs means the
			// next type will be indexed.
			identType = ((ArrayType) identType).getElementType();
		}
		return identType;
	}

	@Override
	public Type visitArray_liter(BasicParser.Array_literContext ctx) {
		List<BasicParser.ExprContext> elems = ctx.expr();
		if (elems.size() == 0) {
			return new ArrayType();
		}

		Type first = visit(ctx.expr(0));
		for (int i = 1; i < elems.size(); i++) {
			// each element must be of same type so can just test against first
			// type.
			if (!visit(elems.get(i)).equalsType(first)) {
				error("Semantic Error at " + ctx.start.getLine() + ":"
						+ ctx.start.getCharPositionInLine()
						+ " -- Inconsistent types in array literal");
			}
		}
		return new ArrayType(first, elems.size());
	}

	@Override
	public Type visitParam(BasicParser.ParamContext ctx) {
		Type type = visit(ctx.type());
		String ident = ctx.IDENT().getText();
		// need to addType param to symbol table so type can be looked up and
		// checked when the function is called.
		symboltable.addIdentifier(ident, new Identifier(type));
		return type;
	}

	@Override
	public Type visitParam_list(Param_listContext ctx) {
		TypeList params = new TypeList();
		for (int i = ctx.param().size() - 1; i >= 0; i--) {
			params.add(visit(ctx.param(i)));
		}
		Collections.reverse(params);
        ctx.paramTypes = params;
		return null;
	}

	@Override
	public Type visitFunc(BasicParser.FuncContext ctx) {
		Type retType = visit(ctx.type());
		SymbolTable symboltable1 = new SymbolTable(symboltable, retType);
		symboltable.setChildren(symboltable1);
		symboltable = symboltable1; // symbol table for parameter list because
									// the whole function has local scope.
		TypeList params = null;
		if (ctx.param_list() != null) {
			visit(ctx.param_list());
            params = ctx.param_list().paramTypes;
		}
		SymbolTable symboltable2 = new SymbolTable(symboltable,
				symboltable.getReturnType());
		symboltable.setChildren(symboltable2);
		symboltable = symboltable2; // the body of the function has its own
									// scope within the whole function scope.
		visit(ctx.stat());
		symboltable = symboltable.getParent().getParent();
		
		String name = ctx.IDENT().getText();
		Function newFunc = new Function(params, retType);
		ctx.name = functionMap.get(name).getFuncName(newFunc);
		return null;
	}

	@Override
	public Type visitProgram(BasicParser.ProgramContext ctx) {
		for (BasicParser.FuncContext func : ctx.func()) {
			Type retType = visit(func.type());
			TypeList paramList = new TypeList();

			if (func.param_list() != null) {
				for (BasicParser.ParamContext param : func.param_list().param()) {
					paramList.add(visit(param.type()));
				}
			}
			Function newFunc = new Function(paramList, retType);
			String funcName = func.IDENT().getText();
			FunctionList funcList = functionMap.get(funcName);
			// functions cannot be redefined.
			if (funcList != null && funcList.hasFunction(newFunc)) {
				error("Semantic Error at " + func.IDENT().getSymbol().getLine()
						+ ":"
						+ func.IDENT().getSymbol().getCharPositionInLine()
						+ " -- Cannot redefine function: "
						+ func.IDENT().getText());
				System.exit(200);
			}

			if (funcList == null) {
				functionMap.put(funcName, new FunctionList(funcName, newFunc));
			} else {
				funcList.add(newFunc);
			}
		}
		visitChildren(ctx);
		symboltable.resetAllChildren();
		return null;
	}

	@Override
	public Type visitPrint(PrintContext ctx) {
		printTypeCheck(visit(ctx.expr()));
		return null;
	}

	private void printTypeCheck(Type exprType) {
		if (exprType.equalsType(PrimitiveTypes.STRING) && !data.hasStringData()) {
			data.setStringData();
		}
		if (exprType.equalsType(PrimitiveTypes.INT) && !data.hasDecimalData()) {
			data.setDecimalData();
		}
		if (exprType.equalsType(PrimitiveTypes.BOOL) && !data.hasBooleanData()) {
			data.setBooleanData();
		}
		if ((exprType.equalsType(new ArrayType(PrimitiveTypes.ANY)) || exprType
				.equalsType(new PairType(PrimitiveTypes.ANY, PrimitiveTypes.ANY)))
				&& !data.hasReferenceData()) {
			data.setReferenceData();
		}
	}

	@Override
	public Type visitPrintln(PrintlnContext ctx) {
		printTypeCheck(visit(ctx.expr()));
		if (!data.hasPrintLn()) {
			data.setPrintln();
		}
		return null;
	}

	@Override
	public Type visitExit(BasicParser.ExitContext ctx) {
		// exit requires an integer exit code.
		if (visit(ctx.expr()) != PrimitiveTypes.INT) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- Expected type: INT, given type: "
					+ visit(ctx.expr()));
			System.exit(200);
		}
		return null;
	}

	@Override
	public Type visitIfstat(BasicParser.IfstatContext ctx) {
		if (visit(ctx.expr()) != PrimitiveTypes.BOOL) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- Expected condition type: BOOL, given type: "
					+ visit(ctx.expr()));
			System.exit(200);
		}
		for (int i = 0; i < 2; i++) { // first loop is for if, second for else.
			// each branch has its own scope and thus symbol table.
			SymbolTable symboltable1 = new SymbolTable(symboltable,
					symboltable.getReturnType());
			symboltable.setChildren(symboltable1);
			symboltable = symboltable1;
			visit(ctx.stat(i));
			symboltable = symboltable.getParent();
		}
		return null;
	}

	@Override
	public Type visitWhiledo(BasicParser.WhiledoContext ctx) {
		if (visit(ctx.expr()) != PrimitiveTypes.BOOL) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- Expected condition type: BOOL, given type: "
					+ visit(ctx.expr()));
			System.exit(200);
		}
		// while loops have own local scope so must have a symbol table.
		SymbolTable symboltable1 = new SymbolTable(symboltable,
				symboltable.getReturnType());
		symboltable.setChildren(symboltable1);
		symboltable = symboltable1;
		visit(ctx.stat());
		symboltable = symboltable.getParent();
		return null;
	}

	@Override
	public Type visitBlock(BasicParser.BlockContext ctx) {
		SymbolTable symboltable1 = new SymbolTable(symboltable,
				symboltable.getReturnType());
		symboltable.setChildren(symboltable1);
		symboltable = symboltable1;
		visit(ctx.stat());
		symboltable = symboltable.getParent();
		return null;
	}

	@Override
	public Type visitAssign_lhs(BasicParser.Assign_lhsContext ctx) {
		Type type;
		if (ctx.IDENT() != null) { // if lhs is a variable, just return the type
									// of that variable.
			String ident = ctx.IDENT().getText();
			Identifier id = this.symboltable.lookupAllLevels(ident);
			if (id == null) {
				error("Semantic Error at " + ctx.start.getLine() + ":"
						+ ctx.start.getCharPositionInLine()
						+ " -- Undefined identifier: " + ident);
				System.exit(200);
			}
			type = id.getType();
		} else if (ctx.array_elem() != null) {
			type = visit(ctx.array_elem());
		} else {
			type = visit(ctx.pair_elem());
		}
		return type;
	}

	@Override
	public Type visitFree(BasicParser.FreeContext ctx) {
		Type exprType = visit(ctx.expr());

		// can only free arrays and pairs.
		if (!(exprType.equalsType(new ArrayType(PrimitiveTypes.ANY)))
				&& !(exprType.equalsType(new PairType(PrimitiveTypes.ANY,
						PrimitiveTypes.ANY)))) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- Expected an Arraytype or frontend.PairType");
			System.exit(200);
		}
		if (!data.hasNullReferenceError()) {
			data.setNullReferenceError();
		}
		return null;
	}

	@Override
	public Type visitArray_type(BasicParser.Array_typeContext ctx) {
		if (ctx.base_type() != null) {
			return new ArrayType(visit(ctx.base_type()));
		} else if (ctx.pair_type() != null) {
			return new ArrayType(visit(ctx.pair_type()));
		} else {
			return new ArrayType(visit(ctx.array_type()));
		}

	}

	@Override
	public Type visitBase_type(BasicParser.Base_typeContext ctx) {
		// determine which type keyword was used and return that type.
		if (ctx.INT() != null) {
			return PrimitiveTypes.INT;
		} else if (ctx.BOOL() != null) {
			return PrimitiveTypes.BOOL;
		} else if (ctx.CHAR() != null) {
			return PrimitiveTypes.CHAR;
		} else {
			return PrimitiveTypes.STRING;
		}
	}

	@Override
	public Type visitPair_elem(BasicParser.Pair_elemContext ctx) {
		Type var = visit(ctx.expr());
		// this visitor is for the fst and snd keywords so the given expression
		// must be a pair.
		if (!(var instanceof PairType)) {
			error("Semantic Error at " + ctx.start.getLine() + ":"
					+ ctx.start.getCharPositionInLine()
					+ " -- Variable is not a frontend.PairType");
			System.exit(200);
		}

		if (!data.hasNullReferenceError()) {
			data.setNullReferenceError();
		}

		PairType p = (PairType) var;
		if (ctx.FST() != null) { // fst keyword was used.
			return p.getFirstType();
		}

		return p.getSecondType();
	}

	// Helper function to print out error messages.
	private void error(String s) {
		System.err.println(s);
	}
}
