package frontend;

public class PairType implements Type {
	private Type firstType;
	private Type secondType;

	public PairType(Type firstType, Type secondType) {
		this.firstType = firstType;
		this.secondType = secondType;
	}

	public Type getFirstType() {
		return firstType;
	}

	public Type getSecondType() {
		return secondType;
	}

    public int getTypeSize(){
    	return 4;
    }
	
	@Override
	public boolean equalsType(Type a) {
		if (a instanceof PairType) {
			PairType a2 = (PairType) a;
            // make sure first types match and second types match.
			return ((this.getFirstType().equalsType(a2.getFirstType())) && this
					.getSecondType().equalsType(a2.getSecondType()));
		}
		if (a instanceof PrimitiveTypes){
			return a == PrimitiveTypes.ANY; // Pair counts as an 'ANY' type.
		}
		return false;
	}

	@Override
	public String toString() {
		return "Pair(" + firstType.toString() + ", " + secondType.toString() + ")";
	}
	
	
}
