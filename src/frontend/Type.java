package frontend;

public interface Type {

	boolean equalsType(Type a);
	int getTypeSize();
}
