package frontend;

import java.util.ArrayList;

public class FunctionList extends ArrayList<Function> {

	private String funcName;

	public FunctionList(String funcName, Function func) {
		this.funcName = funcName;
		this.add(func);

	}

	public String getFuncName(Function otherFunc) {
		int funcIndex = getFunction(otherFunc);
		if (funcIndex == 0) {
			return funcName;
		}
		return funcName + "_" + funcIndex;
	}

	public boolean hasFunction(Function otherFunc) {
		int index = getFunction(otherFunc);
		return index >= 0;
	}

	public TypeList getReturnTypes(TypeList argList) {
		TypeList result = new TypeList();
		for (Function func : this) {
			if (func.argsEqual(argList)) {
				result.add(func.getReturnType());
			}
		}
		return result;
	}

	private int getFunction(Function otherFunc) {
		for (int i = 0; i < this.size(); i++) {
			if (this.get(i).equals(otherFunc)) {
				return i;
			}
		}
		return -1;
	}

	public boolean hasArgLength(int size) {
		for (Function func : this) {
			if (func.getParams().size() == size) {
				return true;
			}
		}
		return false;
	}

	public boolean validArgTypes(TypeList args) {
		for (Function func : this) {
			if (func.argsEqual(args)) {
				return true;
			}
		}
		return false;
	}

}
