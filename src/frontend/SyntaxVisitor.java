package frontend;

import antlr.*;

public class SyntaxVisitor extends BasicParserBaseVisitor<Boolean> {

	@Override
	public Boolean visitProgram(BasicParser.ProgramContext ctx) {
		boolean returns = true;
		for (BasicParser.FuncContext func : ctx.func()) {
			returns &= visit(func); // checking that every function returns.
		}
		if (!returns) {
			System.err.println("No return or exit statement");
			System.exit(100);
		}
		return returns;
	}

	@Override
	public Boolean visitFunc(BasicParser.FuncContext ctx) {
		return visit(ctx.stat());
	}

	@Override
	public Boolean visitIfstat(BasicParser.IfstatContext ctx) {
        // both branches in an if must return in order to say if statement returns.
		return (visit(ctx.stat(0)) && visit(ctx.stat(1)));
	}

	@Override
	public Boolean visitRet(BasicParser.RetContext ctx) {
		return true;
	}

	@Override
	public Boolean visitExit(BasicParser.ExitContext ctx) {
		return true;
	}

	@Override
	public Boolean visitSequence(BasicParser.SequenceContext ctx) {
		return (visit(ctx.stat(0)) || visit(ctx.stat(1)));
	}

	@Override
	public Boolean visitBlock(BasicParser.BlockContext ctx) {
		return visit(ctx.stat());
	}

	@Override
	public Boolean visitSkip(BasicParser.SkipContext ctx) {
		return false;
	}

	@Override
	public Boolean visitFree(BasicParser.FreeContext ctx) {
		return false;
	}

	@Override
	public Boolean visitPrintln(BasicParser.PrintlnContext ctx) {
		return false;
	}

	@Override
	public Boolean visitRead(BasicParser.ReadContext ctx) {
		return false;
	}

	@Override
	public Boolean visitDeclaration(BasicParser.DeclarationContext ctx) {
		return false;
	}

	@Override
	public Boolean visitPrint(BasicParser.PrintContext ctx) {
		return false;
	}

	@Override
	public Boolean visitWhiledo(BasicParser.WhiledoContext ctx) {
		return visit(ctx.stat());
	}

	@Override
	public Boolean visitAssignment(BasicParser.AssignmentContext ctx) {
		return false;
	}
}
