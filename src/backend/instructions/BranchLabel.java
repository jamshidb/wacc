package backend.instructions;

public class BranchLabel extends Branch {

    public BranchLabel(String label) {
        super(label);
    }

    @Override
    public String instruction() {
        return "BL";
    }
}
