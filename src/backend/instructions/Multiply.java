package backend.instructions;

import backend.Operand;

public class Multiply implements Instruction{
	
	private Operand op1;
	private Operand op2;
	private Operand op3;
	private Operand op4;
	
	
	public Multiply(Operand op1, Operand op2, Operand op3, Operand op4) {
		this.op1 = op1;
		this.op2 = op2;
		this.op3 = op3;
		this.op4 = op4;
	}


	@Override
	public String toString(){
		return "SMULL " + op1 + ", " + op2 + ", " + op3 + ", " + op4 + "\n"; 
		
	}

}
