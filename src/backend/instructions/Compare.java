package backend.instructions;

import backend.Operand;

public class Compare implements Instruction{
	private Operand op1;
	private Operand op2;
	
	public Compare(Operand op1, Operand op2) {
		this.op1 = op1;
		this.op2 = op2;
	}
	@Override 
	public String toString(){
		return "CMP " + op1 + ", " + op2 +"\n";
	}
	

}