package backend.instructions;

public class BranchVS extends Branch{

	public BranchVS(String label) {
		super(label);
	}
	
	@Override
	public String instruction() {
		return "BLVS";
	}
	
}
