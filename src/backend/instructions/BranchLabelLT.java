package backend.instructions;

public class BranchLabelLT extends Branch{
    public BranchLabelLT(String label) {
        super(label);
    }

    @Override
    public String instruction() {
        return "BLLT";
    }
}
