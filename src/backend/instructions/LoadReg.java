package backend.instructions;

import backend.Register;

// Loads something into a register
public class LoadReg implements Instruction {
    private int loadType;
    private Register destReg;
    private int value;
    /* for loadType value key:
     * 0 = LDR
     * 1 = LDRSB
     * 2 = LDRNE
     * 3 = LDREQ
     */

    private Register reg2 = null;
    private int stackOffset;

    private Label label = null;


    public LoadReg(Register destReg, int value) {
        this.destReg = destReg;
        this.value = value;
        this.loadType = 0;
    }
    

    public LoadReg(Register destReg, Register reg2, int stackOffset, int loadType) {
        this.destReg = destReg;
        this.reg2 = reg2;
        this.stackOffset = stackOffset;
        this.loadType = loadType;
    }

    public LoadReg(Register destReg, Label label, int loadType) {
        this.destReg = destReg;
        this.label = label;
        this.loadType = loadType;
    }


    @Override
    public String toString() {
    	return instruction() + destReg + ", " + argument();
    }

    private String argument() {
        if (label != null){
            String result = "=" + label;
            return result.replace(":", "");
        } else if (reg2 != null) {
            return  "[" + reg2.name() + (stackOffset == 0? "" : (", #" + stackOffset)) + "]\n";
        } else{
            return "=" + value + "\n";
        }
    }

    private String instruction() {
    	switch(loadType){
    	case(0):
    		return  "LDR ";
        case(1): 
            return  "LDRSB ";
        case(2):
        	return "LDRNE ";
        case(3):
        	return "LDREQ ";
        case(4):
            return "LDRLT ";
         case(5):
            return "LDRCS ";
        default:
        	return "";
    	}
    }

}