package backend.instructions;

import backend.Operand;
import backend.Register;

public class AddSub implements Instruction{
    private Register reg1;
    private Register reg2;
    private Operand op;
    private boolean isAdd;
    private boolean instr;
    private int shift = -1;

    public AddSub(Register reg1, Register reg2, Operand op, boolean isAdd) {
        this.reg1 = reg1;
        this.reg2 = reg2;
        this.op = op;
        this.isAdd = isAdd;
    }

    public AddSub(Register reg1, Register reg2, Operand op, boolean isAdd, boolean instr) {
        this.reg1 = reg1;
        this.reg2 = reg2;
        this.op = op;
        this.isAdd = isAdd;
        this.instr = instr;
    }

    public AddSub(Register reg1, Register reg2, Register reg3, int shift) {
        this.reg1 = reg1;
        this.reg2 = reg2;
        this.op = new Operand(reg3);
        this.shift = shift;
        this.isAdd = true;
    }
    
    @Override
    public String toString() {
        String result = instruction() + reg1 + ", " + reg2 + ", " + op;
        if (shift != -1) {
            result += ", LSL #" + shift;
        }
        return result + "\n";
    }

    private String instruction() {
        if(isAdd && instr){
            return "ADDS ";
        }else if(isAdd && !instr) {
            return  "ADD ";
        }else if(!isAdd && instr) {
            return  "SUBS ";
        }else {
            return  "SUB ";
        }
    }
}