package backend.instructions;

public class Branch implements Instruction {
    protected String label;

    public Branch(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return instruction() + " " + label + "\n";
    }

    public String instruction() {
        return "B";
    }
}
