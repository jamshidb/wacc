package backend.instructions;

public class Label implements Instruction {
	private String label;
	private int index = -1;
	private boolean isFunction;

	public Label(String label) {
		this.label = label;
	}

	public Label(String label, int index) {
		this.label = label;
		this.index = index;
	}

	public Label(boolean isFunction, String label) {
		this.label = label;
		this.isFunction = true;
	}

	public String getName() {
		if (isFunction){
			return "f_";
		}
		return label;
	}

	public int getIndex() {
		return index;
	}
	
	@Override
	public String toString() {
		if (index < 0 ) {
			return label + ":\n";
		}
		else {
			return label + "_" + index + ":\n";
		}
	}
}
