package backend.instructions;


import backend.Register;

public class Negative implements Instruction {
    private Register dest;
    private Register src;

    public Negative(Register dest, Register src) {
        this.dest = dest;
        this.src = src;
    }

    @Override
    public String toString() {
        return "RSBS " + dest + ", " + src + ", #0\n";
    }
}