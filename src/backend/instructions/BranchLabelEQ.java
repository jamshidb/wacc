package backend.instructions;

public class BranchLabelEQ extends Branch {
	
	public BranchLabelEQ(String label) {
        super(label);
    }

    @Override
    public String instruction() {
        return "BLEQ";
    }
	
}
