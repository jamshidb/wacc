package backend.instructions;

import backend.Operand;
import backend.Register;

public class StoreStoreB implements Instruction {
    private Register dest;
    private Register addr1;
    private Operand op1;
    private boolean isStore;

    public StoreStoreB(Register addr1, Register dest, Operand op1, boolean isStore) {
        this.dest = dest;
        this.addr1 = addr1;
        this.op1 = op1;
        this.isStore = isStore;
    }

    @Override
    public String toString() {
        String result;
        
        if(isStore){
        	result =  "STR " ;
        }else{
        	result =  "STRB ";
        }
        result += addr1 + ", [" + dest;
        
        if (op1 != null) {
            result += (op1.getImmValue() == 0? "" : (", " + op1));
        }
        result +=  (op1.getImmValue() < 0? "]!\n" : "]\n");
        return result;
    }
}
