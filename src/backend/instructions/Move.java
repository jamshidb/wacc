
package backend.instructions;

import backend.Operand;

public class Move implements Instruction {

	private Operand op1;
	private Operand op2;
	private boolean normal = true;
	private int binOp;
	
	public Move(Operand op1, Operand op2){
		this.op1 = op1;
		this.op2 = op2;
	}
			
	public Move(Operand op1, Operand op2, int binOp) {
		this.op1 = op1;
		this.op2 = op2;
		this.binOp = binOp;		
		normal = false;
	}
	
	public Move(Operand op1, Operand op2, boolean binOp) {
		this.op1 = op1;
		this.op2 = op2;
		this.binOp = binOp? 5 : 4;
		normal = false;
	}
	
	@Override
    public String toString() {
		if(normal) {
			return "MOV "+ op1 + ", " + op2 + "\n";
		}else {
			String instr = "MOV";
			switch(binOp) {
			case 0:
				instr += "GT ";
				break;
			case 1:
				instr += "GE ";
				break;
			case 2:
				instr += "LT ";
				break;
			case 3:
				instr += "LE ";
				break;
			case 4:
				instr +="EQ ";
				break;
			case 5:
				instr +="NE ";
			}
			instr += op1 + ", " + op2 + "\n";
			return instr;
		}
    }
}
