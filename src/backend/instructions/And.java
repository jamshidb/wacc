package backend.instructions;

import backend.Operand;
import backend.Register;

public class And extends LogicalOperator {
    public And(Register dest, Register left, Operand right) {
        super(right, left, dest);
    }

    @Override
    public String instruction() {
        return  "AND";
    }
}