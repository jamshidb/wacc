package backend.instructions;


import backend.Operand;
import backend.Register;

public class Or extends LogicalOperator {
    public Or(Register dest, Register left, Operand right) {
        super(right, left, dest);
    }

    @Override
    public String instruction() {
        return  "ORR";
    }
}
