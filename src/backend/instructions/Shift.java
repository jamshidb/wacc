package backend.instructions;

import backend.Operand;

public class Shift implements Instruction{
	
	private Operand register;
	private Operand shiftValue;
	
	public Shift(Operand register, Operand shiftValue) {
		this.register = register;
		this.shiftValue = shiftValue;
	}
	
	@Override
	public String toString() {
		return register + ", ASR " + shiftValue;
	}


}
