package backend.instructions;

public interface Instruction {
    String toString();
}
