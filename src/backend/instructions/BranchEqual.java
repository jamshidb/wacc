package backend.instructions;


public class BranchEqual extends Branch {

    public BranchEqual(String label) {
        super(label);
    }

    @Override
    public String instruction() {
        return "BEQ";
    }
}
