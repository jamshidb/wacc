package backend.instructions;

import backend.Operand;
import backend.Register;

public abstract class LogicalOperator implements Instruction {
    protected Register dest;
    protected Register left;
    protected Operand right;

    public LogicalOperator(Operand right, Register left, Register dest) {
        this.right = right;
        this.left = left;
        this.dest = dest;
    }

    @Override
    public String toString() {
        return instruction() + " " + dest.name() + ", " + left.name() + ", " + right + "\n";
    }

    public abstract String instruction();
}
