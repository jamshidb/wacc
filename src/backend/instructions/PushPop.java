package backend.instructions;

import backend.Register;
import java.util.ArrayList;
import java.util.Arrays;

public class PushPop implements Instruction {
    private ArrayList<Register> regs;
    private boolean isPush;
    private int value;

    public PushPop(ArrayList<Register> regs, boolean isPush) {
        this.regs = regs;
        this.isPush = isPush;
    }

    public PushPop(Register reg, boolean isPush) {
        this.regs = new ArrayList<Register>(Arrays.asList(reg));
        this.isPush = isPush;
    }


    @Override
    public String toString() {
    	String result;
    	if(isPush){
            result = "PUSH {";
    	}else{
    		result = "POP {";
    	}
    	if (regs != null) {
            for (Register reg : regs) {
                result += reg + ",";
            }

            result = result.substring(0, result.length() - 1);
        } else {
            result += value;
        }

        return result + "}\n";
    }
}
