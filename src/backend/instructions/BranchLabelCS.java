package backend.instructions;

/**
 * Created by dh1814 on 04/12/14.
 */
public class BranchLabelCS extends Branch {
    public BranchLabelCS(String label) {
        super(label);
    }

    @Override
    public String instruction() {
        return "BLCS";
    }
}
