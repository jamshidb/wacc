package backend.instructions;

import backend.Operand;
import backend.Register;

public class XOr extends LogicalOperator {
    public XOr(Register dest, Register left, Operand right) {
        super(right, left, dest);
    }
    @Override
    public String instruction() {
        return "EOR";
    }
}
