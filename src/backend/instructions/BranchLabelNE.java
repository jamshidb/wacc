package backend.instructions;

public class BranchLabelNE extends Branch{
    
	public BranchLabelNE(String label) {
        super(label);
    }

    @Override
    public String instruction() {
        return "BLNE";
    }

}
