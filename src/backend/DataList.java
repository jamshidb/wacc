package backend;

import java.util.ArrayList;

/* Contains standard strings for errors and printing. */
public class DataList extends ArrayList<String> {
    private boolean hasPrintLn = false;
    private boolean hasStringData = false;
    private boolean hasDecimalData = false;
    private boolean hasBooleanData = false;
    private boolean hasCharData = false;
    private boolean hasArithmeticData = false;
    private int dataIndex = 0;
	private boolean hasModDivData;
    private boolean hasNegArrayErrorData = false;
    private boolean hasArrayOutOfBounds = false;
	private boolean hasReferenceData = false;
    private boolean hasNullReferenceError = false;

    public int getNextDataIndex() {
        return dataIndex++;
    }

    public void setNegArrayErrorData() {
        this.add(dataIndex, "\"ArrayIndexOutOfBoundsError: negative index\\n\\0\"");
        hasNegArrayErrorData = true;
        dataIndex++;
    }
    public boolean hasNegArrayErrorData(){
        return hasNegArrayErrorData;
    }
    public int getNegArrayErrorIndex() {
        return this.indexOf("\"ArrayIndexOutOfBoundsError: negative index\\n\\0\"");
    }

    public void setArrayOutOfBounds(){
        this.add(dataIndex, "\"ArrayIndexOutOfBoundsError: index too large\\n\\0\"");
        hasArrayOutOfBounds = true;
        dataIndex++;
        if (!hasNegArrayErrorData()){
            setNegArrayErrorData();
        }
        if (!hasStringData()) {
            setStringData();
        }
    }
    public boolean hasArrayOutOfBounds(){
        return hasArrayOutOfBounds;
    }
    public int getArrayOutOfBoundsIndex(){
        return this.indexOf("\"ArrayIndexOutOfBoundsError: index too large\\n\\0\"");
    }

    public void setStringData() {
        this.add(dataIndex, "\"%.*s\\0\"");
        hasStringData = true;
        dataIndex++;
    }
    public boolean hasStringData() {
        return hasStringData;
    }
    public int getStringIndex(){
        return this.indexOf("\"%.*s\\0\"");
    }

    public void setBooleanData() {
    	this.add(dataIndex, "\"true\\0\"");
    	this.add(dataIndex, "\"false\\0\"");
    	hasBooleanData = true;
    	dataIndex += 2;
    }
    public boolean hasBooleanData() {
        return hasBooleanData;
    }

    public void setDecimalData() {
    	this.add(dataIndex, "\"%d\\0\"");
    	hasDecimalData = true;
    	dataIndex++;
    }
    public boolean hasDecimalData() {
        return hasDecimalData;
    }
    public int getDecimalIndex(){
        return this.indexOf("\"%d\\0\"");
    }

    public void setPrintln() {
    	this.add(dataIndex, "\"\\0\"");
    	hasPrintLn = true;
    	dataIndex++;
        hasPrintLn = true;
    }
    public boolean hasPrintLn() {
        return hasPrintLn;
    }

    public void setCharData() {
    	this.add(dataIndex, "\" %c\\0\"");
    	dataIndex++;
    	hasCharData = true;
    }
    public boolean hasCharData(){
        return hasCharData;
    }
    public int getCharIndex(){
        return this.indexOf("\" %c\\0\"");
    }

    public void setArithmeticData() {
    	this.add(dataIndex, "\"OverflowError: the result is too small/large to store in a 4-byte signed-integer.\\n\"");
    	dataIndex++;
    	hasArithmeticData  = true;
        if (!hasStringData()) {
            setStringData();
        }
    }
    public boolean hasArithmeticData() {
        return hasArithmeticData;
    }
    public int getArithmeticIndex(){
        return this.indexOf("\"OverflowError: the result is too small/large to store in a 4-byte signed-integer.\\n\"");
    }

    public void setModDivData() {
    	this.add(dataIndex, "\"DivideByZeroError: divide or modulo by zero\\n\\0\"");
    	dataIndex++;
    	hasModDivData  = true;
        if (!hasStringData()) {
            setStringData();
        }
    }
    public boolean hasModDivData() {
        return hasModDivData;
    }
    public int getModDivIndex(){
        return this.indexOf("\"DivideByZeroError: divide or modulo by zero\\n\\0\"");
    }

    public void setReferenceData() {
    	this.add(dataIndex,"\"%p\\0\"");
    	dataIndex++;
    	hasReferenceData = true;
    }
    public boolean hasReferenceData() {
        return hasReferenceData ;
    }
    public int getReferenceIndex() {
        return this.indexOf("\"%p\\0\"");
    }
    
    public int getTruthIndex(){
    	return this.indexOf("\"true\\0\"");
    }

    public int getNewLineIndex(){
    	return this.indexOf("\"\\0\"");
    }

    public int getFalsityIndex(){
    	return this.indexOf("\"false\\0\"");
    }

    public void setNullReferenceError(){
        this.add(dataIndex, "\"NullReferenceError: dereference a null reference\\n\\0\"");
        hasNullReferenceError = true;
        dataIndex++;
        if (!hasStringData) {
            setStringData();
        }
    }
    public boolean hasNullReferenceError(){
        return hasNullReferenceError;
    }
    public int getNullReferenceError() {
        return  this.indexOf("\"NullReferenceError: dereference a null reference\\n\\0\"");
    }


}