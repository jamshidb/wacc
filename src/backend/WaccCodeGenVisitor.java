package backend;

import antlr.BasicParser;
import antlr.BasicParser.Arg_listContext;
import antlr.BasicParser.Assign_lhsContext;
import antlr.BasicParser.BlockContext;
import antlr.BasicParser.ExprBoolLiteralContext;
import antlr.BasicParser.ExprBracketsContext;
import antlr.BasicParser.ExprCharLiteralContext;
import antlr.BasicParser.ExprCompareContext;
import antlr.BasicParser.ExprEqualityContext;
import antlr.BasicParser.ExprMulDivContext;
import antlr.BasicParser.ExprStrLiteralContext;
import antlr.BasicParser.ExprSumDiffContext;
import antlr.BasicParser.FuncContext;
import antlr.BasicParser.Param_listContext;
import antlr.BasicParser.PrintContext;
import antlr.BasicParser.PrintlnContext;
import antlr.BasicParser.ReadContext;
import antlr.BasicParser.RetContext;
import antlr.BasicParser.RhsCallContext;
import antlr.BasicParser.SequenceContext;
import backend.instructions.*;
import frontend.*;
import java.util.ArrayList;


public class WaccCodeGenVisitor extends antlr.BasicParserBaseVisitor<Operand> {
	private DataList data;
	private SymbolTable symbolTable;
	private ArrayList<Instruction> instructions = new ArrayList<Instruction>();
	public static ArrayList<Label> labels = new ArrayList<Label>();
	private LabelManager labelManager;
	private RegisterList registers = new RegisterList();

	public WaccCodeGenVisitor(SymbolTable symbolTable, DataList dataList) {
		this.symbolTable = symbolTable;
		this.data = dataList;
		this.labelManager = new LabelManager(labels,
				symbolTable.getDeclaredBytes(), data);
	}

	public ArrayList<Instruction> getInstructions() {
		return instructions;
	}

    // adds necessary instructions for the standard labels
	public void manageLabels(BasicParser.ProgramContext ctx) {
		for (int i = 0; i < labels.size(); i++) {
			switch (labels.get(i).getName()) {
			case "msg":
				instructions.addAll(labelManager.messageManage(i));
				break;
			case "main":
				instructions.add(new Directive("text"));
				instructions.add(new Directive("global main"));

				for (int j = 0; j < ctx.func().size(); j++) {
					visit(ctx.func(j));
					instructions.add(new Label("f_" + ctx.func(j).name));
					instructions.add(new PushPop(Register.lr, true));
					symbolTable = symbolTable.getChild();
					int neededBytes = symbolTable.getDeclaredBytes();
					addSubScopeBytes(false, neededBytes);

					visit(ctx.func(j).stat());
					registers.setUsedUpTo(Register.r4.ordinal());
					symbolTable = symbolTable.getParent().getParent();
					instructions.add(new PushPop(Register.pc, false));
					instructions.add(new Directive("ltorg"));
				}
				instructions.addAll(labelManager.mainManager(i, false));
				visit(ctx.stat());
				instructions.addAll(labelManager.mainManager(i, true));
				break;
			case "p_print_string":
				instructions.addAll(labelManager.printStringManage(i));
				break;
			case "p_print_ln":
				labelManager.printlnManage(instructions, i);
				break;
			case "p_print_bool":
				labelManager.printBoolManage(instructions, i);
				break;
			case "p_print_int":
				labelManager.printIntManage(instructions, i);
				break;
			case "p_read_int":
				labelManager.readManage(instructions, i, true);
				break;
			case "p_read_char":
				labelManager.readManage(instructions, i, false);
				break;
			case "p_throw_overflow_error":
				labelManager.overflowErrorManage(instructions, i);
				break;
			case "p_throw_runtime_error":
				labelManager.runtimeErrorManage(instructions, i);
				break;
			case "p_check_divide_by_zero":
				labelManager.divideByZeroManage(instructions, i);
				break;
			case "p_check_array_bounds":
				labelManager.checkArrayBoundsManage(instructions, i);
				break;
			case "p_print_reference":
				labelManager.printReferenceManage(instructions, i);
				break;
			case "p_check_null_pointer":
				labelManager.checkNullPointerManage(instructions, i);
				break;
			case "p_free_pair":
				labelManager.freePair(instructions, i);
				break;
			case "p_free_array":
				labelManager.freeArray(instructions, i);
				break;
			}
		}
	}

	private void dataManager() {
		if (!data.isEmpty()) {
			instructions.add(new Directive("data"));
		}
		for (int i = 0; i < data.size(); i++) {
			labels.add(new Label("msg", i));
		}
	}

	@Override
	public Operand visitProgram(BasicParser.ProgramContext ctx) {
		dataManager();
		labels.add(StandardLabels.MAIN.getLabel());
		manageLabels(ctx);
		return null;
	}

	@Override
	public Operand visitExit(BasicParser.ExitContext ctx) {
		Register exitValue = visit(ctx.expr()).getRegister();
		// move the exit value into the argument register
		instructions.add(new Move(new Operand(Register.r0), new Operand(
				exitValue)));
		instructions.add(new BranchLabel("exit"));
		return null;
	}

	@Override
	public Operand visitRet(RetContext ctx) {
		Operand returnValue = visit(ctx.expr());
		instructions.add(new Move(new Operand(Register.r0), returnValue));
		addSubScopeBytes(true, symbolTable.getCumulativeBytes());
		instructions.add(new PushPop(Register.pc, false));
		return null;
	}

	@Override
	public Operand visitExprIntLiteral(BasicParser.ExprIntLiteralContext ctx) {
		int value = Integer.parseInt(ctx.INT_LITER().getText());
		Register freeReg = registers.getNext();
		instructions.add(new LoadReg(freeReg, value));
		return new Operand(freeReg, PrimitiveTypes.INT);
	}

	@Override
	public Operand visitExprBoolLiteral(ExprBoolLiteralContext ctx) {
		int bool; // integer representation of bool to be put in register
		Register freeReg = registers.getNext();
		if (ctx.BOOL_LITER().getText().equals("false")) {
			bool = 0;
		} else {
			bool = 1;
		}
		instructions.add(new Move(new Operand(freeReg), new Operand(bool)));
		return new Operand(freeReg, PrimitiveTypes.BOOL);
	}

	public Operand visitExprStrLiteral(ExprStrLiteralContext ctx) {
		Register freeReg = registers.getNext();
		instructions.add(new LoadReg(freeReg, labels.get(data
				.getNextDataIndex()), 0));
		return new Operand(freeReg, PrimitiveTypes.STRING);
	}

	@Override
	public Operand visitExprIdent(BasicParser.ExprIdentContext ctx) {
		String idName = ctx.IDENT().getText();
		Identifier id = symbolTable.lookupAllLevels(idName);

		Register freeReg = registers.getNext();
		int pos = symbolTable.getOffset(idName);
		int loadSB;
		if (isBoolOrChar(id.getType())) {
			loadSB = 1;
		} else {
			loadSB = 0;
		}

		instructions.add(new LoadReg(freeReg, Register.sp, pos, loadSB));
		return new Operand(freeReg, id.getType());
	}

	@Override
	public Operand visitExprCharLiteral(ExprCharLiteralContext ctx) {
		String character = ctx.CHAR_LITER().getText();
		Register freeReg = registers.getNext();
		instructions
				.add(new Move(new Operand(freeReg), new Operand(character)));
		return new Operand(freeReg, PrimitiveTypes.CHAR);
	}

	@Override
	public Operand visitPrint(PrintContext ctx) {
		Operand expr = visit(ctx.expr());
		basicPrint(expr.getRegister(), expr.getType());
		return null;
	}

	@Override
	public Operand visitPrintln(PrintlnContext ctx) {
		Operand expr = visit(ctx.expr());
		basicPrint(expr.getRegister(), expr.getType());
		instructions.add(new BranchLabel(StandardLabels.PRINTLN.getName()));
		if (!labels.contains(StandardLabels.PRINTLN.getLabel())) {
			labels.add(StandardLabels.PRINTLN.getLabel());
		}
		return null;
	}

    // Adds needed labels for each print type
	private void basicPrint(Register currentReg, Type type) {
		String printType;

		if (type.equalsType(new ArrayType(PrimitiveTypes.CHAR))) {
			type = PrimitiveTypes.STRING;
		}
		if (!type.equalsType(PrimitiveTypes.CHAR)) {
			StandardLabels label;
			if (type.equalsType(PrimitiveTypes.BOOL)) {
				printType = StandardLabels.PRINT_BOOL.getName();
				label = StandardLabels.PRINT_BOOL;
			} else if (type.equalsType(PrimitiveTypes.INT)) {
				printType = StandardLabels.PRINT_INT.getName();
				label = StandardLabels.PRINT_INT;
			} else if (type.equalsType(PrimitiveTypes.STRING)) {
				printType = StandardLabels.PRINT_STRING.getName();
				label = StandardLabels.PRINT_STRING;
			} else {
				printType = StandardLabels.PRINT_LABEL.getName();
				label = StandardLabels.PRINT_LABEL;
			}
			if (!labels.contains(label.getLabel())) {
				labels.add(label.getLabel());
			}
		} else {
			printType = "putchar";
		}
		instructions.add(new Move((new Operand(Register.r0)), new Operand(
				currentReg)));
		instructions.add(new BranchLabel(printType));
		registers.setUsedUpTo(currentReg.ordinal());
	}

	@Override
	public Operand visitRead(ReadContext ctx) {
		Operand operand = visit(ctx.assign_lhs());
		Type type = operand.getType();
		Register currentReg = operand.getRegister();
		String readType;
		StandardLabels label;
		if (type.equalsType(PrimitiveTypes.INT)) {
			readType = StandardLabels.READ_INT.getName();
			label = StandardLabels.READ_INT;
		} else {
			readType = StandardLabels.READ_CHAR.getName();
			label = StandardLabels.READ_CHAR;
		}
		if (!labels.contains(label.getLabel())) {
			labels.add(label.getLabel());
		}
		instructions.add(new Move((new Operand(Register.r0)), new Operand(
				currentReg)));
		instructions.add(new BranchLabel(readType));
		registers.setUsedUpTo(currentReg.ordinal());
		return null;
	}

	@Override
	public Operand visitFree(BasicParser.FreeContext ctx) {
		Operand expr = visit(ctx.expr());
		instructions.add(new Move(new Operand(Register.r0), expr));
		if (expr.getType() instanceof PairType) {
			instructions
					.add(new BranchLabel(StandardLabels.FREE_PAIR.getName()));
			addNeededLabels(StandardLabels.FREE_PAIR);
		} else {
			instructions.add(new BranchLabel(StandardLabels.FREE_ARRAY
					.getName()));
			addNeededLabels(StandardLabels.FREE_ARRAY);
		}
		return null;
	}

	@Override
	public Operand visitAssign_lhs(Assign_lhsContext ctx) {
		if (ctx.IDENT() != null) {
			String idName = ctx.IDENT().getText();
			Identifier id = symbolTable.lookupAllLevels(idName);
			Register freeReg = registers.getNext();

			int pos = symbolTable.getOffset(idName);
			instructions.add(new AddSub(freeReg, Register.sp, new Operand(pos),
					true));

			return new Operand(freeReg, id.getType());
		}

		return super.visitAssign_lhs(ctx);
	}

	@Override
	public Operand visitIfstat(BasicParser.IfstatContext ctx) {
		Register conditional = visit(ctx.expr()).getRegister();
		String elseLabel = labelManager.getNewLabel();
		String endLabel = labelManager.getNewLabel();
		// Branch to else if conditional evaluates to 0
		instructions.add(new Compare(new Operand(conditional), new Operand(0)));
		registers.setUsedUpTo(conditional.ordinal());
		instructions.add(new BranchEqual(elseLabel));
		symbolTable = symbolTable.getChild();

		int declaredBytes = symbolTable.getDeclaredBytes();
		addSubScopeBytes(false, declaredBytes);
		visit(ctx.stat(0));
		addSubScopeBytes(true, declaredBytes);
		symbolTable = symbolTable.getParent();
		registers.setUsedUpTo(Register.r4.ordinal());
		instructions.add(new Branch(endLabel));

		instructions.add(new Label(elseLabel));
		symbolTable = symbolTable.getChild();
		declaredBytes = symbolTable.getDeclaredBytes();
		addSubScopeBytes(false, declaredBytes);
		visit(ctx.stat(1));
		addSubScopeBytes(true, declaredBytes);

		symbolTable = symbolTable.getParent();
		registers.setUsedUpTo(Register.r4.ordinal());
		instructions.add(new Label(endLabel));
		return null;
	}

	@Override
	public Operand visitExprEquality(ExprEqualityContext ctx) {
		int firstMove = 4;
		int secondMove = 5;
		if (ctx.NEQ() != null) {
			firstMove = 5;
			secondMove = 4;
		}
		Register reg1 = visit(ctx.expr(0)).getRegister();
		Register reg2 = visit(ctx.expr(1)).getRegister();
		instructions.add(new Compare(new Operand(reg1), new Operand(reg2)));
		instructions
				.add(new Move(new Operand(reg1), new Operand(1), firstMove));
		instructions
				.add(new Move(new Operand(reg1), new Operand(0), secondMove));
		return new Operand(reg1, PrimitiveTypes.BOOL);
	}

	@Override
	public Operand visitExprSumDiff(ExprSumDiffContext ctx) {
		Operand reg1 = visit(ctx.expr(0));
		Operand reg2;
		if (!registers.canGetNext()) {
			instructions.add(new PushPop(reg1.getRegister(), true));
		}
		reg2 = visit(ctx.expr(1));
		boolean add = false;
		if (ctx.PLUS() != null) {
			add = true;
		}
		if (reg1.getRegister().toString().equals(reg2.toString())) {
			instructions.add(new PushPop(Register.r11, false));
			instructions.add(new AddSub(reg1.getRegister(), Register.r11, reg1,
					add, true));
		} else {
			instructions.add(new AddSub(reg1.getRegister(), reg1.getRegister(),
					reg2, add, true));
		}
		instructions.add(new BranchVS(StandardLabels.OVERFLOW_ERROR.getName()));
		addNeededLabels(StandardLabels.OVERFLOW_ERROR);

		return reg1;
	}

	@Override
	public Operand visitExprMulDiv(ExprMulDivContext ctx) {
		Operand reg1 = visit(ctx.expr(0));
		Operand reg2 = visit(ctx.expr(1));
		if (ctx.MULT() != null) {
			instructions.add(new Multiply(reg1, reg2, reg1, reg2));
			instructions.add(new Compare(new Operand(reg2.getRegister()),
					new Operand(reg1.getRegister(), 31)));
			instructions.add(new BranchLabelNE(StandardLabels.OVERFLOW_ERROR
					.getName()));
			addNeededLabels(StandardLabels.OVERFLOW_ERROR);
		} else if (ctx.DIVIDE() != null) {
			instructions.add(new Move(new Operand(Register.r0), reg1));
			instructions.add(new Move(new Operand(Register.r1), reg2));
			instructions.add(new BranchLabel(StandardLabels.CHECK_ZERO_DIVIDE
					.getName()));
			instructions.add(new BranchLabel("__aeabi_idiv"));
			instructions.add(new Move(reg1, new Operand(Register.r0)));
			addNeededLabels(StandardLabels.CHECK_ZERO_DIVIDE);
		} else {
			instructions.add(new Move(new Operand(Register.r0), reg1));
			instructions.add(new Move(new Operand(Register.r1), reg2));
			instructions.add(new BranchLabel(StandardLabels.CHECK_ZERO_DIVIDE
					.getName()));
			instructions.add(new BranchLabel("__aeabi_idivmod"));
			instructions.add(new Move(reg1, new Operand(Register.r1)));
			addNeededLabels(StandardLabels.CHECK_ZERO_DIVIDE);
		}
		return reg1;
	}

	@Override
	public Operand visitWhiledo(BasicParser.WhiledoContext ctx) {
		String conditionLabel = labelManager.getNewLabel();
		String bodyLabel = labelManager.getNewLabel();
		instructions.add(new Branch(conditionLabel));

		instructions.add(new Label(bodyLabel));
		symbolTable = symbolTable.getChild();
		int declaredBytes = symbolTable.getDeclaredBytes();
		addSubScopeBytes(false, declaredBytes);
		visit(ctx.stat());
		addSubScopeBytes(true, declaredBytes);
		symbolTable = symbolTable.getParent();

		instructions.add(new Label((conditionLabel)));
		Register condition = visit(ctx.expr()).getRegister();
		// if condition is true, branch up to body
		instructions.add(new Compare(new Operand(condition), new Operand(1)));
		registers.setUsedUpTo(condition.ordinal());
		instructions.add(new BranchEqual(bodyLabel));

		return null;
	}

	@Override
	public Operand visitParam_list(Param_listContext ctx) {
		symbolTable = symbolTable.getChild();

		String ident;
		Identifier id;
		for (int i = ctx.param().size() - 1; i >= 0; i--) {
			ident = ctx.param(i).IDENT().getText();
			id = symbolTable.lookupCurrLevel(ident);
			id.setDeclared(true);
		}
		symbolTable.addIdentifier("1", new Identifier(PrimitiveTypes.INT));
		return null;
	}

	@Override
	public Operand visitDeclaration(BasicParser.DeclarationContext ctx) {
		String ident = ctx.IDENT().getText();
		Identifier id = symbolTable.lookupCurrLevel(ident);
		id.setDeclared(true);
		Type type = id.getType();
		boolean isStore = !isBoolOrChar(type);
		int bytes = bytesNeeded(type);
		if (!isStore) {
			// char or bool
			bytes = 1;
		} else {
			bytes = 4;
		}

		Register result;
		if (type instanceof ArrayType
				&& ((ctx.assign_rhs()).getChild(0) instanceof BasicParser.Array_literContext)) {
			// array could be declared equal to an array literal or a pair
			// element
			// so must only declareArray if it's actually an array literal
			result = declareArray(id, ctx);
		} else {
			result = visit(ctx.assign_rhs()).getRegister();
		}

		// Store variable into next open spot in stack.
		Operand stackAddress = new Operand(symbolTable.getOffset(ident));
		instructions.add(new StoreStoreB(result, Register.sp, stackAddress,
				isStore));

		registers.setUsedUpTo(result.ordinal());

		return null;
	}

	@Override
	public Operand visitBlock(BlockContext ctx) {
		symbolTable = symbolTable.getChild();
		int neededBytes = symbolTable.getDeclaredBytes();
		if (neededBytes > 0) {
			instructions.add(new AddSub(Register.sp, Register.sp, new Operand(
					neededBytes), false));
		}
		visit(ctx.stat());
		if (neededBytes > 0) {
			instructions.add(new AddSub(Register.sp, Register.sp, new Operand(
					neededBytes), true));
		}
		symbolTable = symbolTable.getParent();
		return null;
	}

	private int bytesNeeded(Type type) {
		if (isBoolOrChar(type)) {
			return 1;
		} else {
			return 4;
		}
	}

	private Register declareArray(Identifier id,
			BasicParser.DeclarationContext ctx) {
		ArrayType type = (ArrayType) id.getType();
		Type innerType = type.getElementType();
		int elemBytes = bytesNeeded(innerType);
		BasicParser.Array_literContext arrayCtx = (BasicParser.Array_literContext) ctx
				.assign_rhs().getChild(0);
		int length = arrayCtx.expr().size();
		elemBytes = length * elemBytes + 4; // 4 for array address
		if (isBoolOrChar(innerType)) {
			instructions.add(new LoadReg(Register.r0, elemBytes));
		} else {
			instructions.add(new LoadReg(Register.r0, elemBytes));
		}
		instructions.add(new BranchLabel("malloc"));
		Register address = registers.getNext();
		instructions.add(new Move(new Operand(address),
				new Operand(Register.r0)));

		visit(ctx.assign_rhs());
		Register reg = registers.getNext();
		instructions.add(new LoadReg(reg, length));
		instructions.add(new StoreStoreB(reg, address, new Operand(0), true));
		return address;
	}

	@Override
	public Operand visitSequence(SequenceContext ctx) {
		visit(ctx.stat(0));
		registers.setUsedUpTo(Register.r4.ordinal());
		visit(ctx.stat(1));
		return null;
	}

	@Override
	public Operand visitAssignment(BasicParser.AssignmentContext ctx) {
		Operand rhs = visit(ctx.assign_rhs());
		Register result = rhs.getRegister();
		Assign_lhsContext lhs = ctx.assign_lhs();

		Register address;
		String ident;
		Type type;
		Identifier id;
		int offset;
		if (lhs.IDENT() != null) {
			ident = ctx.assign_lhs().IDENT().getText();
			address = Register.sp;
			id = symbolTable.lookupAllLevels(ident);
			type = id.getType();
			offset = symbolTable.getOffset(ident);
		} else if (lhs.array_elem() != null) {
			ident = ctx.assign_lhs().array_elem().IDENT().getText();
			address = visit(ctx.assign_lhs().array_elem()).getRegister();
			id = symbolTable.lookupAllLevels(ident);
			type = id.getType();
			if (type.equalsType(PrimitiveTypes.STRING)) {
				type = PrimitiveTypes.CHAR;
			}
			offset = 0;
		} else {
			Operand pair = visit(ctx.assign_lhs());
			address = pair.getRegister();
			type = pair.getType();
			offset = 0;
		}

		boolean isStore = !isBoolOrChar(type);
		instructions.add(new StoreStoreB(result, address, new Operand(offset),
				isStore));
		registers.setUsedUpTo(result.ordinal());
		return null;
	}

	private boolean isBoolOrChar(Type type) {
		if (type.equalsType(PrimitiveTypes.BOOL)
				|| type.equalsType(PrimitiveTypes.CHAR)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Operand visitExprAnd(BasicParser.ExprAndContext ctx) {
		Register left = visit(ctx.expr(0)).getRegister();
		Register right = visit(ctx.expr(1)).getRegister();
		// the result can go in left because don't need to save left's value
		instructions.add(new And(left, left, new Operand(right)));
		registers.setUsedUpTo(left.ordinal());
		return new Operand(left, PrimitiveTypes.BOOL);
	}

	@Override
	public Operand visitExprOR(BasicParser.ExprORContext ctx) {
		Register left = visit(ctx.expr(0)).getRegister();
		Register right = visit(ctx.expr(1)).getRegister();
		instructions.add(new Or(left, left, new Operand(right)));
		registers.setUsedUpTo(left.ordinal());
		return new Operand(left, PrimitiveTypes.BOOL);
	}

	@Override
	public Operand visitExprUnary(BasicParser.ExprUnaryContext ctx) {
		Register currReg = visit(ctx.expr()).getRegister();
		Type type = PrimitiveTypes.ANY;
		if (ctx.unary().NOT() != null) {
			instructions.add(new XOr(currReg, currReg, new Operand(1)));
			type = PrimitiveTypes.BOOL;
		} else if (ctx.unary().MINUS() != null) {
			instructions.add(new Negative(currReg, currReg));
			type = PrimitiveTypes.INT;
			instructions.add(new BranchVS(StandardLabels.OVERFLOW_ERROR
					.getName()));
			addNeededLabels(StandardLabels.OVERFLOW_ERROR);

		} else if (ctx.unary().LEN() != null) {
			instructions.add(new LoadReg(currReg, currReg, 0, 0));
			type = PrimitiveTypes.INT;
		} else if (ctx.unary().ORD() != null) {
			type = PrimitiveTypes.INT;
		} else if (ctx.unary().CHR() != null) {
			type = PrimitiveTypes.CHAR;
		}
		// Don't need cases and chr because no instructions produced for
		// those.
		return new Operand(currReg, type);
	}

	@Override
	public Operand visitArray_liter(BasicParser.Array_literContext ctx) {
		// array literals only appear in declarations and thus the address of
		// the array is in the last used register.
		Register address = registers.getLastUsed();
		int size = ctx.expr().size();
		if (size == 0) {
			return new Operand(address, new ArrayType());
		}
		// Figure out element type to determine bytes needed for each store
		Operand first = visit(ctx.expr(0));
		Type elemType = first.getType();
		int bytes = bytesNeeded(elemType);
		Operand offset = new Operand(4);
		instructions.add(new StoreStoreB(first.getRegister(), address, offset,
				!isBoolOrChar(elemType)));
		registers.setUsedUpTo(first.getRegister().ordinal());

		int j = 1;
		for (int i = 1; i < size; i++) {
			Register result = visit(ctx.expr(i)).getRegister();
			offset = new Operand(((j++) * bytes) + 4);
			instructions.add(new StoreStoreB(result, address, offset,
					!isBoolOrChar(elemType)));
			registers.setUsedUpTo(result.ordinal());
		}

		return new Operand(address, new ArrayType(elemType, size));
	}

	@Override
	public Operand visitRhsPair(BasicParser.RhsPairContext ctx) {
		instructions.add(new LoadReg(Register.r0, 8)); // 8 because pairs hold
														// two addresses
		instructions.add(new BranchLabel("malloc"));
		Register address = registers.getNext();
		instructions.add(new Move(new Operand(address),
				new Operand(Register.r0)));

		for (int i = 0; i < 2; i++) {
			Operand result = visit(ctx.expr(i));
			Type type = result.getType();
			int bytes = bytesNeeded(type);
			instructions.add(new LoadReg(Register.r0, bytes));
			instructions.add(new BranchLabel("malloc"));
			Operand offset = new Operand(i * 4);
			instructions.add(new StoreStoreB(result.getRegister(), Register.r0,
					new Operand(0), !isBoolOrChar(type)));
			instructions
					.add(new StoreStoreB(Register.r0, address, offset, true));
			registers.setUsedUpTo(result.getRegister().ordinal());
		}

		return new Operand(address, new PairType(PrimitiveTypes.ANY,
				PrimitiveTypes.ANY));
	}

	@Override
	public Operand visitExprCompare(ExprCompareContext ctx) {
		Operand left = visit(ctx.expr(0));
		Operand right = visit(ctx.expr(1));

		int cmp = 0;
		if (ctx.GT() != null) {
			cmp = 0;
		} else if (ctx.GE() != null) {
			cmp = 1;
		} else if (ctx.LT() != null) {
			cmp = 2;
		} else if (ctx.LE() != null) {
			cmp = 3;
		}

		int sndCmp = 0;
		if (ctx.GT() != null) {
			sndCmp = 3;
		} else if (ctx.GE() != null) {
			sndCmp = 2;
		} else if (ctx.LT() != null) {
			sndCmp = 1;
		} else if (ctx.LE() != null) {
			sndCmp = 0;
		}

		instructions.add(new Compare(left, right));
		instructions.add(new Move(left, new Operand(1), cmp));
		instructions.add(new Move(left, new Operand(0), sndCmp));
		return new Operand(left.getRegister(), PrimitiveTypes.BOOL);
	}

	@Override
	public Operand visitExprBrackets(ExprBracketsContext ctx) {
		return visit(ctx.expr());

	}

	@Override
	public Operand visitArray_elem(BasicParser.Array_elemContext ctx) {
		String ident = ctx.IDENT().getText();
		Identifier id = symbolTable.lookupAllLevels(ident);
		int offset = symbolTable.getOffset(ident);
		Register address = registers.getNext();
		instructions.add(new AddSub(address, Register.sp, new Operand(offset),
				true));

		Type idType = id.getType();
		Operand index;
		for (BasicParser.ExprContext expr : ctx.expr()) {
			index = visit(expr);
			if (id.getType().equalsType(PrimitiveTypes.STRING)) {
				idType = PrimitiveTypes.CHAR;
			} else {
				idType = ((ArrayType) idType).getElementType();
			}
			instructions.add(new LoadReg(address, address, 0, 0));
			instructions.add(new Move(new Operand(Register.r0), index));
			instructions.add(new Move(new Operand(Register.r1), new Operand(
					address)));
			instructions.add(new BranchLabel(StandardLabels.ARRAY_BOUNDS_CHECK
					.getName()));

			instructions
					.add(new AddSub(address, address, new Operand(4), true));
			if (!isBoolOrChar(idType)) {
				instructions.add(new AddSub(address, address, index
						.getRegister(), 2));
			} else {
				instructions.add(new AddSub(address, address, index, true));
			}

			registers.setUsedUpTo(index.getRegister().ordinal());
		}

		if (!(ctx.getParent() instanceof BasicParser.Assign_lhsContext)) {
			if (isBoolOrChar(idType)) {
				instructions.add(new LoadReg(address, address, 0, 1));

			} else {
				instructions.add(new LoadReg(address, address, 0, 0));
			}
		}

		addNeededLabels(StandardLabels.ARRAY_BOUNDS_CHECK);
		return new Operand(address, idType);
	}

	@Override
	public Operand visitFunc(FuncContext ctx) {
		if (ctx.param_list() != null) {
			visit(ctx.param_list());
		} else {
			symbolTable = symbolTable.getChild();
			symbolTable.addIdentifier("1", new Identifier(PrimitiveTypes.INT));
		}

		return null;
	}

	@Override
	public Operand visitRhsCall(RhsCallContext ctx) {
		int argOffSet = 0;
		if (ctx.arg_list() != null) {
			argOffSet = visit(ctx.arg_list()).getImmValue();
		}

		instructions.add(new BranchLabel("f_" + ctx.funcName));
		registers.setUsedUpTo(Register.r4.ordinal());

		instructions.add(new AddSub(Register.sp, Register.sp, new Operand(
				argOffSet), true));
		Register nextReg = registers.getNext();
		instructions.add(new Move(new Operand(nextReg),
				new Operand(Register.r0)));
		return new Operand(nextReg, PrimitiveTypes.INT);
	}

	@Override
	public Operand visitArg_list(Arg_listContext ctx) {
		int argOffset;
		int totalSize = 0;

		for (int i = ctx.expr().size() - 1; i >= 0; i--) {
			Operand argument = visit(ctx.expr(i));
			boolean isBoolChar = isBoolOrChar(argument.getType());

			argOffset = argument.getType().getTypeSize();
			totalSize += argOffset;
			instructions.add(new StoreStoreB(argument.getRegister(),
					Register.sp, new Operand(-argOffset), !isBoolChar));
			symbolTable.addCumulativeArgs(argOffset);
			registers.setUsedUpTo(Register.r4.ordinal());
		}

		symbolTable.resetCumulativeArgs();
		return new Operand(totalSize);
	}

	@Override
	public Operand visitExprPairLiteral(BasicParser.ExprPairLiteralContext ctx) {
		Register nullPair = registers.getNext();
		instructions.add(new LoadReg(nullPair, 0));
		return new Operand(nullPair, new PairType(PrimitiveTypes.ANY,
				PrimitiveTypes.ANY));
	}

	@Override
	public Operand visitPair_elem(BasicParser.Pair_elemContext ctx) {
		Operand address = visit(ctx.expr());
		instructions.add(new Move(new Operand(Register.r0), address));
		instructions.add(new BranchLabel(StandardLabels.CHECK_NULL_POINTER
				.getName()));
		addNeededLabels(StandardLabels.CHECK_NULL_POINTER);

		int offset = 0, loadKey = 0;
		Type elemType;
		if (ctx.SND() != null) {
			offset = 4;
			elemType = ((PairType) address.getType()).getSecondType();
		} else {
			elemType = ((PairType) address.getType()).getFirstType();
		}

		if (isBoolOrChar(elemType)) {
			loadKey = 1;
		}

		instructions.add(new LoadReg(address.getRegister(), address
				.getRegister(), offset, 0));
		if (!(ctx.getParent() instanceof Assign_lhsContext)) {
			instructions.add(new LoadReg(address.getRegister(), address
					.getRegister(), 0, loadKey));
		}

		return new Operand(address.getRegister(), elemType);
	}

	// adds or subtracts specified bytes from stack pointer
	private void addSubScopeBytes(boolean isAdd, int neededBytes) {
		if (neededBytes > 0) {
			instructions.add(new AddSub(Register.sp, Register.sp, new Operand(
					neededBytes), isAdd));
		}
	}

	// Helper function that adds correct labels for each standard label type
	private void addNeededLabels(StandardLabels label) {
		switch (label.getName()) {
		case "p_print_string":
			if (!labels.contains(StandardLabels.PRINT_STRING.getLabel())) {
				labels.add(StandardLabels.PRINT_STRING.getLabel());
			}
			break;
		case "p_throw_overflow_error":
			if (!labels.contains(StandardLabels.OVERFLOW_ERROR.getLabel())) {
				labels.add(StandardLabels.OVERFLOW_ERROR.getLabel());
			}
			addNeededLabels(StandardLabels.RUNTIME_ERROR);
			break;
		case "p_throw_runtime_error":
			if (!labels.contains(StandardLabels.RUNTIME_ERROR.getLabel())) {
				labels.add(StandardLabels.RUNTIME_ERROR.getLabel());
			}
			addNeededLabels(StandardLabels.PRINT_STRING);
			break;
		case "p_check_divide_by_zero":
			if (!labels.contains(StandardLabels.CHECK_ZERO_DIVIDE.getLabel())) {
				labels.add(StandardLabels.CHECK_ZERO_DIVIDE.getLabel());
			}
			addNeededLabels(StandardLabels.RUNTIME_ERROR);
			break;
		case "p_check_array_bounds":
			if (!labels.contains(StandardLabels.ARRAY_BOUNDS_CHECK.getLabel())) {
				labels.add(StandardLabels.ARRAY_BOUNDS_CHECK.getLabel());
			}
			addNeededLabels(StandardLabels.RUNTIME_ERROR);
			break;
		case "p_check_null_pointer":
			if (!labels.contains(StandardLabels.CHECK_NULL_POINTER.getLabel())) {
				labels.add(StandardLabels.CHECK_NULL_POINTER.getLabel());
			}
			addNeededLabels(StandardLabels.RUNTIME_ERROR);
			break;
		case "p_free_pair":
			if (!labels.contains(StandardLabels.FREE_PAIR.getLabel())) {
				labels.add(StandardLabels.FREE_PAIR.getLabel());
			}
			addNeededLabels(StandardLabels.RUNTIME_ERROR);
			break;
		case "p_free_array":
			if (!labels.contains(StandardLabels.FREE_ARRAY.getLabel())) {
				labels.add(StandardLabels.FREE_ARRAY.getLabel());
			}
			addNeededLabels(StandardLabels.RUNTIME_ERROR);
			break;
		}
	}
}
