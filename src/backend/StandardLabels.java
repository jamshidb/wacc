package backend;

import backend.instructions.Label;

public enum StandardLabels {
	
	MAIN(new Label("main")), 
	PRINTLN(new Label("p_print_ln")),
	PRINT_STRING(new Label("p_print_string")),
	PRINT_BOOL(new Label("p_print_bool")),
	PRINT_INT(new Label("p_print_int")),
	READ_INT(new Label("p_read_int")),
	READ_CHAR(new Label("p_read_char")),
	OVERFLOW_ERROR(new Label("p_throw_overflow_error")),
	RUNTIME_ERROR(new Label("p_throw_runtime_error")),
	CHECK_ZERO_DIVIDE(new Label("p_check_divide_by_zero")),
    ARRAY_BOUNDS_CHECK( new Label("p_check_array_bounds")),
    PRINT_LABEL(new Label("p_print_reference")),
    CHECK_NULL_POINTER(new Label("p_check_null_pointer")),
    FREE_PAIR(new Label("p_free_pair")),
    FREE_ARRAY(new Label("p_free_array"));
	
	private Label label;
	
	private StandardLabels(Label label){
		this.label = label;
		
	}
	
	public Label getLabel(){
		return label;
	}

    public String getName() {
        return label.getName();
    }
	
}
