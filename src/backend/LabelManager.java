package backend;

import java.util.ArrayList;
import backend.instructions.*;

public class LabelManager {
	private ArrayList<Label> labels;
	private DataList data; // a ArrayList of Strings;
	private int declaredBytes;
    private int newLabelCount = 0;

    // Adds instructions for standard labels
	public LabelManager(ArrayList<Label> labels, int declaredBytes, DataList data) {
		this.labels = labels;
		this.data = data;
		this.declaredBytes = declaredBytes;
	}

    public String getNewLabel() {
        return "L" + (newLabelCount++);
    }
	
	public ArrayList<Instruction> mainManager(int i, boolean isAdd) {
		int maxStackInstr = 1024;
        int neededBytes = declaredBytes;
        ArrayList<Instruction> instructions = new ArrayList<Instruction>();
		if (!isAdd){
			instructions.add(labels.get(i));
			instructions.add(new PushPop(Register.lr, true));
		}

		while (neededBytes > maxStackInstr) {
			instructions.add(new AddSub(Register.sp, Register.sp, new Operand(
					maxStackInstr), isAdd));
            neededBytes -= maxStackInstr;
		}

		if (neededBytes > 0) {
			instructions.add(new AddSub(Register.sp, Register.sp, new Operand(
                    neededBytes), isAdd));
		}
		
		// main automatically returns 0
		if (isAdd){
			instructions.add(new LoadReg(Register.r0, 0));
			instructions.add(new PushPop(Register.pc, false));
			instructions.add(new Directive("ltorg"));
		}
        return instructions;
	}
	

	public ArrayList<Instruction> messageManage(int i) {
        int length = 0;
        for (int j = 0; j < data.get(i).length(); j ++) {
            if (data.get(i).charAt(j) != '\\') {
                length++;
            }
        }
        ArrayList<Instruction> instructions = new ArrayList<Instruction>();
		instructions.add(labels.get(i));
		instructions.add(new Directive("word " + (length-2)));
		instructions.add(new Directive("ascii " + data.get(i)));
        return instructions;
	}
	
	public ArrayList<Instruction> printStringManage(int i) {
        ArrayList<Instruction> instructions = new ArrayList<Instruction>();
		instructions.add(labels.get(i));
		instructions.add(new PushPop(Register.lr, true));
		instructions.add(new LoadReg(Register.r1, Register.r0, 0, 0 ));
		instructions.add(new AddSub(Register.r2, Register.r0, new Operand(4), true));
		instructions.add(new LoadReg(Register.r0, labels.get(data.getStringIndex()), 0));
		instructions.add(new AddSub(Register.r0, Register.r0, new Operand(4), true));
		instructions.add(new BranchLabel("printf"));
		instructions.add(new Move(new Operand(Register.r0), new Operand(0)));
		instructions.add(new BranchLabel("fflush"));
		instructions.add(new PushPop(Register.pc, false));
        return instructions;
	}
	
	public void printlnManage(ArrayList<Instruction> instructions, int i){
		instructions.add(labels.get(i));
		instructions.add(new PushPop(Register.lr, true));
		instructions.add(new LoadReg(Register.r0, labels.get(data.getNewLineIndex()), 0));
		instructions.add(new AddSub(Register.r0, Register.r0, new Operand(4), true));
		instructions.add(new BranchLabel("puts"));
		instructions.add(new Move(new Operand(Register.r0), new Operand(0)));
		instructions.add(new BranchLabel("fflush"));
		instructions.add(new PushPop(Register.pc, false));
	}


	public void printBoolManage(ArrayList<Instruction> instructions, int i) {
		instructions.add(labels.get(i));
		instructions.add(new PushPop(Register.lr, true));
		instructions.add(new Compare(new Operand(Register.r0), new Operand(0)));
		instructions.add(new LoadReg(Register.r0, labels.get(data.getTruthIndex()), 2));
		instructions.add(new LoadReg(Register.r0, labels.get(data.getFalsityIndex()), 3));
		instructions.add(new AddSub(Register.r0, Register.r0, new Operand(4), true));
		instructions.add(new BranchLabel("printf"));
		instructions.add(new Move(new Operand(Register.r0), new Operand(0)));
		instructions.add(new BranchLabel("fflush"));
		instructions.add(new PushPop(Register.pc, false));
	}

	public void printIntManage(ArrayList<Instruction> instructions, int i) {
		instructions.add(labels.get(i));
		instructions.add(new PushPop(Register.lr, true));
		instructions.add(new Move(new Operand(Register.r1), new Operand(Register.r0)));
		instructions.add(new LoadReg(Register.r0, labels.get(data.getDecimalIndex()), 0));
		instructions.add(new AddSub(Register.r0, Register.r0, new Operand(4), true));
		instructions.add(new BranchLabel("printf"));
		instructions.add(new Move(new Operand(Register.r0), new Operand(0)));
		instructions.add(new BranchLabel("fflush"));
		instructions.add(new PushPop(Register.pc, false));
	}

	public void printReferenceManage(ArrayList<Instruction> instructions, int i) {
		instructions.add(labels.get(i));
		instructions.add(new PushPop(Register.lr, true));
		instructions.add(new Move(new Operand(Register.r1), new Operand(Register.r0)));
		instructions.add(new LoadReg(Register.r0, labels.get(data.getReferenceIndex()), 0));
		instructions.add(new AddSub(Register.r0, Register.r0, new Operand(4), true));
		instructions.add(new BranchLabel("printf"));
		instructions.add(new Move(new Operand(Register.r0), new Operand(0)));
		instructions.add(new BranchLabel("fflush"));
		instructions.add(new PushPop(Register.pc, false));
	}
	
	public void readManage(ArrayList<Instruction> instructions, int i, boolean isIntManage) {
		int dataIndex;
		if (isIntManage){
			dataIndex = data.getDecimalIndex();
		}else{
			dataIndex = data.getCharIndex();
		}
		instructions.add(labels.get(i));
		instructions.add(new PushPop(Register.lr, true));
		instructions.add(new Move(new Operand(Register.r1), new Operand(Register.r0)));
		instructions.add(new LoadReg(Register.r0, labels.get(dataIndex), 0));
		instructions.add(new AddSub(Register.r0, Register.r0, new Operand(4), true));
		instructions.add(new BranchLabel("scanf"));
		instructions.add(new PushPop(Register.pc, false));
	}

	public void overflowErrorManage(ArrayList<Instruction> instructions, int i) {
		instructions.add(labels.get(i));
		instructions.add(new LoadReg(Register.r0, labels.get(data.getArithmeticIndex()), 0));
		instructions.add(new BranchLabel(StandardLabels.RUNTIME_ERROR.getName()));
	}

	public void runtimeErrorManage(ArrayList<Instruction> instructions, int i) {
		instructions.add(labels.get(i));
		instructions.add(new BranchLabel(StandardLabels.PRINT_STRING.getName()));
		instructions.add(new Move(new Operand(Register.r0), new Operand(-1)));
		instructions.add(new BranchLabel("exit"));
	}
	
	public void divideByZeroManage(ArrayList<Instruction> instructions, int i) {
		instructions.add(labels.get(i));
		instructions.add(new PushPop(Register.lr, true));
		instructions.add(new Compare(new Operand(Register.r1), new Operand(0)));
		instructions.add(new LoadReg(Register.r0, labels.get(data.getModDivIndex()), 3));
		instructions.add(new BranchLabelEQ(StandardLabels.RUNTIME_ERROR.getName()));
		instructions.add(new PushPop(Register.pc, false));
	}

    public void checkArrayBoundsManage(ArrayList<Instruction> instructions, int i) {
        instructions.add(labels.get(i));
        instructions.add(new PushPop(Register.lr, true));
        instructions.add(new Compare(new Operand(Register.r0), new Operand(0)));
        instructions.add(new LoadReg(Register.r0, labels.get(data.getNegArrayErrorIndex()), 4));
        instructions.add(new BranchLabelLT(StandardLabels.RUNTIME_ERROR.getName()));
        instructions.add(new LoadReg(Register.r1, Register.r1, 0, 0));
        instructions.add(new Compare(new Operand(Register.r0), new Operand(Register.r1)));
        instructions.add(new LoadReg(Register.r0, labels.get(data.getArrayOutOfBoundsIndex()), 5));
        instructions.add(new BranchLabelCS(StandardLabels.RUNTIME_ERROR.getName()));
        instructions.add(new PushPop(Register.pc, false));
    }

    public void checkNullPointerManage(ArrayList<Instruction> instructions, int i) {
        instructions.add(labels.get(i));
        instructions.add(new PushPop(Register.lr, true));
        instructions.add(new Compare(new Operand(Register.r0), new Operand(0)));
        instructions.add(new LoadReg(Register.r0, labels.get(data.getNullReferenceError()), 3));
        instructions.add(new BranchLabelEQ(StandardLabels.RUNTIME_ERROR.getName()));
        instructions.add(new PushPop(Register.pc, false));
    }

    public void freePair(ArrayList<Instruction> instructions, int i) {
        instructions.add(labels.get(i));
        instructions.add(new PushPop(Register.lr, true));
        instructions.add(new Compare(new Operand(Register.r0), new Operand(0)));
        instructions.add(new LoadReg(Register.r0, labels.get(data.getNullReferenceError()), 3));
        instructions.add(new BranchLabelEQ(StandardLabels.RUNTIME_ERROR.getName()));
        instructions.add(new PushPop(Register.r0, true));

        instructions.add(new LoadReg(Register.r0, Register.r0, 0, 0));
        instructions.add(new BranchLabel("free"));
        instructions.add(new LoadReg(Register.r0, Register.sp, 0, 0));
        instructions.add(new LoadReg(Register.r0, Register.r0, 4, 0));
        instructions.add(new BranchLabel("free"));

        instructions.add(new PushPop(Register.r0, false));
        instructions.add(new BranchLabel("free"));
        instructions.add(new PushPop(Register.pc, false));
    }

    public void freeArray(ArrayList<Instruction> instructions, int i) {
        instructions.add(labels.get(i));
        instructions.add(new PushPop(Register.lr, true));
        instructions.add(new Compare(new Operand(Register.r0), new Operand(0)));
        instructions.add(new LoadReg(Register.r0, labels.get(data.getNullReferenceError()), 3));
        instructions.add(new BranchLabelEQ(StandardLabels.RUNTIME_ERROR.getName()));
        instructions.add(new BranchLabel("free"));
        instructions.add(new PushPop(Register.pc, false));
    }

}