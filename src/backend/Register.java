package backend;

/*
 * R0 - Arg0, function return value
 * R1 - Arg1, second 32 bits of return value if double/int
 * R2-R3 - Args 2-3
 * R4-R10 - Permanent registers
 * R11 - Permanent register, ARM frame pointer
 * R12 - temporary register
 * R13 - stack pointer, permanent register
 * R14 - link register, permanent register
 * R15 - program counter
 */

public enum Register {
    r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, sp, lr, pc;


    public String toString() {
        return this.name();
    }
}
