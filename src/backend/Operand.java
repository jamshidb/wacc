package backend;

import backend.instructions.Shift;
import frontend.Type;

public class Operand {

	private Register register;
	private int immValue;
	private String str;
	private Type type;
	private Shift shift;
	private int shiftAmount;
    // string should be one character long

	public Operand(Register register){
		this.str = null;
        this.register = register;
	}
	
	public Operand(Register register, int shiftAmount){
		this.register = register;
		shift = new Shift(new Operand(register), new Operand(shiftAmount));

	}
	
	public Operand(Register register, int immValue, boolean b){
		this.register = register;
		this.immValue = immValue;
	}
	
	public Operand(Register register, Type type){
		this.str = null;
        this.register = register;
        this.type = type;
	}
	
	public int getImmValue() {
		return immValue;
	}
	
	public Operand(int immValue){
        this.str = null;
        register = null;
		this.immValue = immValue;
	}

    public Operand(String str) {
        this.register = null;
        this.str = str;
    }
    
    public Register getRegister(){
    	return register;
    }
    
    public Type getType(){
    	return type;
    }

    @Override
	public String toString(){
		if(register == null && str == null){
			//operand is ImmValue;
			return "#" + Integer.toString(immValue);
		} else if (str != null) {
            return "#" + str;
        } else if (shift != null){
			return shift.toString();
		} else {
			return register.toString();
		}
	}
}
