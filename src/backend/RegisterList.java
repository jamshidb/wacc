package backend;

/* Class representing list of ARM registers that keeps track of which registers have been used.
 * 
 * R0 - Arg0, function return value
 * R1 - Arg1, second 32 bits of return value if double/int
 * R2-R3 - Args 2-3 
 * R4-R10 - Permanent registers
 * R11 - Permanent register, ARM frame pointer
 * R12 - temporary register
 * R13 - stack pointer, permanent register
 * R14 - link register, permanent register
 * R15 - program counter
 */
public class RegisterList {
	private int maxReg = 15;
    private int testReg = 10;
	private int usedUpTo = 4; // Starts with 4 because 0 - 3 are reserved
    private boolean canGetNext = true;

	public Register getNext() throws IndexOutOfBoundsException {
		if (usedUpTo >= testReg) {
            canGetNext = false;
            return Register.r10;
		} else {
            return Register.values()[(usedUpTo++) % testReg];
        }
	}



    public boolean canGetNext(){
        return canGetNext;
    }

    // must be set to a value between 4 and 15.
    public void setUsedUpTo(int i) {
        usedUpTo = i;
    }

    public Register getLastUsed() {
        return Register.values()[usedUpTo - 1];
    }

}
