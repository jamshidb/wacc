parser grammar BasicParser;

@header {
package antlr;
import frontend.Type;
import frontend.TypeList;
}

options {
  tokenVocab=BasicLexer;
}

program : BEGIN (func)* stat END EOF;

func locals[String name = ""] : type IDENT OPEN_PARENTHESES param_list? CLOSE_PARENTHESES IS stat END;

param_list locals[TypeList paramTypes]: param (COMMA param)*;

param : type IDENT;

stat : SKIP					                #skip

       | type IDENT ASSIGN assign_rhs		#declaration

       | assign_lhs ASSIGN assign_rhs		#assignment

       | READ assign_lhs			        #read

       | FREE expr				            #free

       | RETURN expr				        #ret

       | EXIT expr				            #exit
 
       | PRINT expr				            #print

       | PRINTLN expr				        #println

       | IF expr THEN stat ELSE stat FI		#ifstat

       | WHILE expr DO stat DONE		    #whiledo

       | BEGIN stat END				        #block

       | stat SEMI_COLON stat			    #sequence
;

 
assign_lhs : IDENT

   | array_elem	

   | pair_elem
;

assign_rhs locals[String funcName="", TypeList returnTypes] :
    expr                                                        #rhsExpr

   | array_liter                                                 #rhsArray

   | NEWPAIR OPEN_PARENTHESES expr COMMA expr CLOSE_PARENTHESES	 #rhsPair

   | pair_elem                                                   #rhsPairElem

   | CALL IDENT OPEN_PARENTHESES arg_list? CLOSE_PARENTHESES     #rhsCall
;



arg_list : expr ( COMMA expr )*;

pair_elem : (FST | SND) expr   
 
;

type : base_type    

     | array_type  
     | pair_type  
;

base_type : INT | CHAR | BOOL | STRING;

array_type : base_type LEFT_SQUARE RIGHT_SQUARE 
           | pair_type LEFT_SQUARE RIGHT_SQUARE
           | array_type LEFT_SQUARE RIGHT_SQUARE
;


pair_type : PAIR OPEN_PARENTHESES pair_elem_type COMMA pair_elem_type CLOSE_PARENTHESES;

pair_elem_type : base_type       

               | array_type 

               | PAIR           
;

unary : NOT | MINUS | LEN | ORD | CHR;

expr : OPEN_PARENTHESES expr CLOSE_PARENTHESES     #exprBrackets

   | INT_LITER                                     #exprIntLiteral

   | BOOL_LITER                                    #exprBoolLiteral

   | CHAR_LITER                                    #exprCharLiteral

   | STR_LITER                                     #exprStrLiteral

   | PAIR_LITER                                    #exprPairLiteral

   | IDENT                                         #exprIdent

   | array_elem                                    #exprArrayElement

   | unary expr                                    #exprUnary
   
   | expr (DIVIDE | MULT | MODULUS) expr           #exprMulDiv
   
   | expr (PLUS | MINUS) expr                      #exprSumDiff

   | expr (GT | GE | LT | LE) expr                 #exprCompare

   | expr (EQ | NEQ) expr                          #exprEquality
  
   | expr AND expr                                 #exprAnd
    
   | expr OR expr                                  #exprOR
 
;


array_elem : IDENT (LEFT_SQUARE expr RIGHT_SQUARE)+    
;	

array_liter : LEFT_SQUARE ( expr (COMMA expr)* )? RIGHT_SQUARE;

