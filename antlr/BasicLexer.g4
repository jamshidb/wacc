lexer grammar BasicLexer;

@header {
package antlr;
}


COMMENT : '#' .*? '\r'? '\n' -> skip;

COMMA : ',' ;


DIVIDE : '/';
MULT : '*';
MODULUS : '%';
PLUS : '+';
MINUS : '-';
GT : '>';
GE : '>=';
LT : '<';
LE : '<=';
EQ : '==';
NEQ : '!=';
AND : '&&';
OR : '||';
NOT : '!';
LEN : 'len';
ORD : 'ord';
CHR : 'chr';
WS: [ \t\n\r\ ] -> skip;

fragment EOL : '\r'? '\n';

ASSIGN: '=';



OPEN_PARENTHESES : '(' ;

CLOSE_PARENTHESES : ')' ;


OPEN_BRACE : '{';

CLOSE_BRACE : '}';


BEGIN: 'begin';
END: 'end';
SKIP: 'skip';
READ: 'read';
IS: 'is';
FREE: 'free';
RETURN: 'return';
EXIT: 'exit';
PRINT: 'print';
PRINTLN: 'println';

IF: 'if';
THEN: 'then';
ELSE: 'else';
FI: 'fi';
WHILE: 'while';
DO: 'do';
DONE: 'done';
SEMI_COLON: ';';
NEWPAIR: 'newpair';
CALL: 'call';
FST: 'fst';
SND: 'snd';
PAIR: 'pair';
LEFT_SQUARE: '[';
RIGHT_SQUARE: ']';

INT: 'int';
BOOL: 'bool';
CHAR: 'char';
STRING: 'string';

INT_LITER : ('+'|'-')?DIGIT+ {Long.valueOf(getText()) <= 2147483647 && Long.valueOf(getText()) >= -2147483648}?;

BOOL_LITER : 'true' | 'false';

CHAR_LITER : '\'' (CHARACTER|ESCAPED_CHAR) '\'';

STR_LITER : '\"' (CHARACTER|ESCAPED_CHAR|'|')* '\"' ;

ESCAPED_CHAR : '\\0' | '\\b' | '\\t' | '\\n' | '\\f' | '\\r' | '\\\"' | '\\\'' 
              | '\\\\';

PAIR_LITER : 'null';

IDENT : (UND | LETTERS) (UND | LETTERS | DIGIT)* ;

fragment UND : '_' ;
fragment LOWER_LETTERS : [a-z] ;
fragment UPPER_LETTERS : [A-Z] ;
fragment LETTERS : LOWER_LETTERS | UPPER_LETTERS ;

fragment DIGIT : [0-9] ;


fragment CHARACTER : ~('\''|'\"'|'\\') | ESCAPED_CHAR;



